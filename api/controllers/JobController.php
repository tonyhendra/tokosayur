<?php
namespace api\controllers;
use Yii;
use backend\models\RuteDelivery;
use backend\models\RutePickup;
use yii\filters\auth\QueryParamAuth;

class JobController extends \yii\rest\Controller
{
	public function behaviors(){
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
    }

	protected function verbs()
    {
       return [
           'index' => ['GET']
       ];
    }
 
  public function actionIndex()
    {
      $id_driver = !empty($_GET['id_driver'])?$_GET['id_driver']:'';
      $model1 = RutePickup::find()->select(['COUNT(*) as count_pickup'])->where(['id_driver'=>$id_driver,'status'=> "Released"])->asArray()->all();
      $model2 = RuteDelivery::find()->select(['COUNT(*) as count_delivery'])->where(['id_driver'=>$id_driver,'status'=> "Released"])->asArray()->all();
      return array_merge($model1,$model2);
    }

}