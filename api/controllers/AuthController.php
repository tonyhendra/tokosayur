<?php
namespace api\controllers;
use Yii;
use backend\models\UserDetails;
use common\models\User;


class AuthController extends \yii\rest\Controller
{
	protected function verbs(){
		return [
			'login' => ['POST'],
			'cek' => ['GET']
		];
	}

	public function actionLogin(){
		$username = !empty($_POST['username'])?$_POST['username']:'';
		$password = !empty($_POST['password'])?$_POST['password']:'';
		$response = [];
		//validasi jika kosong
		if(empty($username) || empty($password)){
			$response = [
				'status' => 'error',
				'message' => 'username & password tidak boleh kosong!',
				'data' => '',
			];
		}
		else{
			//caro didatabase
			$user = \common\models\User::findByUsername($username);
			//jika username ada maka
			if(!empty($user)){
				if($user->validatePassword($password)){
					$user_details = \backend\models\UserDetails::findOne(["username" => $username]);
					$response = [
						'status' => 'success',
						'message' => 'berhasil!',
						'data' => [
							'id' => $user->id,
							'username' => $user->username,
							'nama' => $user_details->nama,
							'email' => $user->email,
							'no_hp' => $user_details->no_hp,
							'tanggal_lahir' => $user_details->tanggal_lahir,
							'jenis_kelamin' =>$user_details->jenis_kelamin,
							'role' => $user->role,
							//token
							'token' => $user->auth_key,

						]
					];
				}
				else{
					$response = [
						'status' => 'error',
						'message' => 'username atau password tidak cocok',
						'data' => '',
					];
				}
			}
		}
		return $response;
	}
	public function actionRegister(){
		$response = [];
        $model = new \backend\models\AddUser();
        $UserDetails = new \backend\models\UserDetails();
        $nama = !empty($_POST['nama'])?$_POST['nama']:'';
        $jenis_kelamin = !empty($_POST['jenis_kelamin'])?$_POST['jenis_kelamin']:'';
        $tanggal_lahir = !empty($_POST['tanggal_lahir'])?$_POST['tanggal_lahir']:'';
		$nohp = !empty($_POST['no_hp'])?$_POST['no_hp']:'';
        
        // load data dari POST request
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        
        if($user = $model->addUser()){
        	$UserDetails->username = $model->username;
        	$UserDetails->nama = $nama;
        	$UserDetails->jenis_kelamin = $jenis_kelamin;
        	$UserDetails->tanggal_lahir = $tanggal_lahir;
        	$UserDetails->no_hp = $nohp;
        	$UserDetails->save();
        	$response = [
        		'status' => 'success',
        		'message' => 'register berhasil',
        		//'data' => $user
        	];
        }else{
        	$response = [
        		'status' => 'error',
        		'message' => 'registrasi gagal',
        	];
        }
        return $response;
        
	}

	public function actionCek(){
		$username = Yii::$app->getRequest()->getQueryParam('username');
		$response = [];
		if(empty($username)){
			$response = [
				'status' => 'error',
				'message' => 'username tidak boleh kosong!',
			];
		}else{
			$user = \common\models\User::findByUsername($username);
			//jika username ada maka
			if(empty($user)){
				$response = [
						'status' => 'success',
						'message' => 'username tersedia',
				];
			}else{
				$response = [
						'status' => 'error',
						'message' => 'username tidak tersedia',
				];
			}

		}
		return $response;


	}

	public function actionUpdate(){

		$username = !empty($_POST['username'])?$_POST['username']:'';
        $model = \common\models\User::findByUsername($username);
        $modelDetails = UserDetails::find()->where(['username' => $username])->one();

		$nama = !empty($_POST['nama'])?$_POST['nama']:'';
		if(!empty($nama)){
	        if($modelDetails->nama!=$nama){
	            $modelDetails->nama = $nama;
	            $modelDetails->update();

	        }
	    }
        $email = !empty($_POST['email'])?$_POST['email']:'';
        if(!empty($email)){
        	if($model->email!=$email){
	        	$model->email = $email;
	        	$model->update();
        	}	
        }
        
        $nohp = !empty($_POST['nohp'])?$_POST['nohp']:'';
        if(!empty($nohp)){
        	if($modelDetails->nohp!=$nohp){
	        	$modelDetails->nohp = $nohp;
	        	$modelDetails->update();
        	}
        }
        $tanggal_lahir = !empty($_POST['tanggal_lahir'])?$_POST['tanggal_lahir']:'';
        if(!empty($tanggal_lahir)){
        	if($modelDetails->tanggal_lahir!=$tanggal_lahir){
        		$modelDetails->tanggal_lahir = $tanggal_lahir;
        		$modelDetails->update();
        	}
        }

        $jenis_kelamin = !empty($_POST['jenis_kelamin'])?$_POST['jenis_kelamin']:'';
        if(!empty($jenis_kelamin)){
        	if($modelDetails->jenis_kelamin!=$jenis_kelamin){
        		$modelDetails->jenis_kelamin = $jenis_kelamin;
        		$modelDetails->update();
        	}
        }
        $password = !empty($_POST['password'])?$_POST['password']:'';

        if(!empty($password)){
	        if($model->getPassword()!=$password){
	        	$model->setPassword($password);
	        	$model->update();
	        }
        }
        
        $response = [
                'status' => 'success',
                'message' => 'berhasil',
            ];

        return $response;
	}


}