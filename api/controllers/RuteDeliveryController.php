<?php
namespace api\controllers;
use Yii;
use backend\models\KonfirmasiPembayaran;
use backend\models\Notifikasi;
use backend\models\Orders;
use backend\models\RuteDelivery;
use yii\filters\auth\QueryParamAuth;

class RuteDeliveryController extends \yii\rest\Controller
{
	public function behaviors(){
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
    }

	protected function verbs()
    {
       return [
           'index' => ['GET'],'detail' => ['GET'], 'konfirmasi' => ['POST']
       ];
    }
 
  public function actionIndex()
    {
        $id_driver = !empty($_GET['id_driver'])?$_GET['id_driver']:'';
        $model = RuteDelivery::find()->where(['id_driver'=>$id_driver])->orderBy(['tanggal'=>SORT_DESC])->all();
        return $model;
    }
  public function actionDetail()
    {
      $id_rute = !empty($_GET['id_rute'])?$_GET['id_rute']:'';
      $model = RuteDelivery::find()->where(['id_rute'=>$id_rute])->asArray()->one();
      $rute = json_decode($model["rute"]);
      $rute_detail = [];
      for($i=0; $i<count($rute);$i++){
        $rute_detail[$i]["id"] = $rute[$i];
        $rute_detail[$i]["alamat"] = RuteDelivery::getAlamat($rute[$i]);
        $rute_detail[$i]["nama"] = RuteDelivery::getNama($rute[$i]);
        $rute_detail[$i]["product"] = RuteDelivery::getNamaProduct($rute[$i]);
        $rute_detail[$i]["jumlah"] = RuteDelivery::getJumlah($rute[$i]);
      }
      //for($i=0; $i<count($rute_detail);$i++){
        $model["rute_detail"] = $rute_detail; 
      //}
      
      return $model["rute_detail"];
    }

    public function actionKonfirmasi()
    {
      $id_rute = !empty($_POST['id_rute'])?$_POST['id_rute']:'';
      $model = RuteDelivery::find()->where(['id_rute'=>$id_rute])->asArray()->one();
      $model1 = RuteDelivery::findOne($id_rute);
      $model1->status = "Completed";
      $model1->save();
      $rute = json_decode($model["rute"]);
      for($i=0; $i<count($rute);$i++){
        $order = Orders::findOne($rute[$i]);
        $order->status = "Completed";
        $order->save();
        $notifikasi = new Notifikasi();
        $notifikasi->id_order = $id_order;
        $notifikasi->id_customer = $order->id_customer;
        $notifikasi->tanggal = date('Y-m-d H:i:s');
        $msg = "Pesanan untuk order number : ".$id_order." stelah berubah status menjadi Completed.";
        $notifikasi->pesan = $msg;
        $this->sendNotification($msg);
            
        $notifikasi->save();
        $result = [
          'status' => "success",
          'message' => "success"
        ];
      }

      return $result;
    }

    public function sendNotification( $message)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $msg = array
        (
            'body'  => "$message",
            'title'     => "Notifikasi"
            );
        $fields = array
        (
            'to'  => "/topics/customer",
            'notification'      => $msg
            );
        $headers = array(
            'Authorization:key = AAAAIzDq1fA:APA91bE8SLpph88dmlCQ5ZWAkdzVjtCamPAvyLqW1SkduuDdQwoT6dB6NitAT-w8rI_RRNqWke24WuvTOoVwdp1HLM_uYcdsqttn-kLbnwT7l0il7g_AU26SH3TRxFyREMzgRcSrQHly',
            'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);           
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}