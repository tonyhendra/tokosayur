<?php
namespace api\controllers;
use Yii;
use backend\models\RutePickup;
use yii\filters\auth\QueryParamAuth;

class RutePickupController extends \yii\rest\Controller
{
	public function behaviors(){
	    $behaviors = parent::behaviors();
	    $behaviors['authenticator'] = [
	    	'class' => QueryParamAuth::className(),
	    ];
	    return $behaviors;
    }

	protected function verbs()
    {
       return [
           'index' => ['GET'],'detail' => ['GET']
       ];
    }
 
    public function actionIndex(){
        $id_driver = !empty($_GET['id_driver'])?$_GET['id_driver']:'';
        $model = RutePickup::find()->where(['id_driver'=>$id_driver])->orderBy(['tanggal'=>SORT_DESC])->all();
        return $model;
    }

    public function actionDetail(){
    	$id_rute = !empty($_GET['id_rute'])?$_GET['id_rute']:'';
        $model = RutePickup::find()->where(['id_rute'=>$id_rute])->asArray()->one();
        $rute = json_decode($model["rute"]);
        $rute_detail = [];
        for($i=0; $i<count($rute);$i++){
        	$rute_detail[$i]["id"] = $rute[$i];
        	$rute_detail[$i]["alamat"] = RutePickup::getAlamat($rute[$i]);
        	$rute_detail[$i]["nama"] = RutePickup::getNama($rute[$i]);
	        $rute_detail[$i]["product"] = RutePickup::getProduct($rute[$i]);
	        $rute_detail[$i]["jumlah"] = RutePickup::getJumlah($rute[$i]);
        }
        //for($i=0; $i<count($rute_detail);$i++){
        	$model["rute_detail"] = $rute_detail;	
        //}
        
        return $model["rute_detail"];
    }

}