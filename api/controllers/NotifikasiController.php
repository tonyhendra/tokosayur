<?php
namespace api\controllers;
use Yii;
use backend\models\Notifikasi;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;

class NotifikasiController extends \yii\rest\Controller
{

    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            //'except' => ['create']
        ];
        return $behaviors;
    }

    protected function verbs()
    {
       return [
           'index' => ['GET'],
       ];
    }
 
    public function actionIndex(){
        $id_customer = !empty($_GET['id_customer'])?$_GET['id_customer']:'';
        $model = Notifikasi::find()->where(['id_customer'=>$id_customer])->orderBy(['tanggal'=>SORT_DESC])->limit(20)->all();
         if(count($model)!=0){ 
            $response = $model;
        }else{
            $response = [
                'status' => 'success',
                'message' => 'Tidak ada notifikasi',
            ];
        }
        return $response;
    }
}