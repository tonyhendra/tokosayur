<?php
namespace api\controllers;
use Yii;
use backend\models\Orders;
use backend\models\OrderDetails;
use backend\models\Product;
use yii\filters\auth\QueryParamAuth;
use yii\helpers\ArrayHelper;

class OrdersController extends \yii\rest\Controller
{

    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => QueryParamAuth::className(),
            //'except' => ['create']
        ];
        return $behaviors;
    }

    protected function verbs()
    {
       return [
           'index' => ['GET'], 'create' => ['POST'], 'detail-order' => ['GET']
       ];
    }
 
    public function actionIndex(){
        $id_customer = !empty($_GET['id_customer'])?$_GET['id_customer']:'';
        $model = Orders::find()->where(['id_customer'=>$id_customer])->orderBy(['id_order'=>SORT_DESC])->all();
         if(count($model)!=0){ 
            $response = $model;
        }else{
            $response = [
                'status' => 'success',
                'message' => 'Belum ada pesanan',
            ];
        }
        return $response;
    }

    public function actionCreate(){
        $id_customer = !empty($_POST['id_customer'])?$_POST['id_customer']:'';
        $alamat = !empty($_POST['alamat'])?$_POST['alamat']:'';
        $lat = !empty($_POST['lat'])?$_POST['lat']:'';
        $lng = !empty($_POST['lng'])?$_POST['lng']:'';
        $total_berat = !empty($_POST['total_berat'])?$_POST['total_berat']:'';
        $total_harga = !empty($_POST['total_harga'])?$_POST['total_harga']:'';
        $id_product = !empty($_POST['id_product'])?$_POST['id_product']:'';
        $jumlah = !empty($_POST['jumlah'])?$_POST['jumlah']:'';

        $orders = new \backend\models\Orders();
        $orderDetails = new \backend\models\OrderDetails();
        $response = [];

        //$orders->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($orders->load(Yii::$app->getRequest()->getBodyParams(), '')) {
            $orders->id_customer = $id_customer;
            $orders->alamat = $alamat;
            $orders->lat = $lat;
            $orders->lng = $lng;
            $orders->total_berat = $total_berat;
            $orders->total_harga = $total_harga;
            $orders->tanggal = date("Y-m-d");
            $orders->status = "Menunggu Pembayaran";
            //$orders->save();
            $transaction = \Yii::$app->db->beginTransaction();
            if($flag = $orders->save(false)){
                $orderDetails->id_order = $orders->id_order;
                $orderDetails->id_product = $id_product;
                $orderDetails->jumlah = $jumlah;
                $product = Product::find()->where(['id_product'=>$id_product])->one();
                $product->stock = $product->stock-$jumlah;
                $product->save();
                $response = [
                    "status" => "success"
                ];
                if(!$flag = $orderDetails->save(false)){
                    $transaction->rollBack();
                    $response = [
                        "status" => "error"
                    ];
                }

            }
            if($flag){
                $transaction->commit();
            }
            // if($orders->save()){
            //     $orderDetails->id_order = $orders->id_order;
            //     $orderDetails->id_product = $id_product;
            //     $orderDetails->jumlah = $jumlah;
            //     $orderDetails->save();
            //     $product = Product::find()->where(['id_product'=>$id_product])->one();
            //     $product->stock = $product->stock-$jumlah;
            //     $product->save();
            //     $response = [
            //         "status" => "success"
            //     ];
            // }else{
            //     $response = [
            //         "status" => "error"
            //     ];
            // }
        }

        return $response;


    }

    public function actionDetailOrder(){
        $id_order = !empty($_GET['id_order'])?$_GET['id_order']:'';
        $model = Orders::find()->where(['id_order'=>$id_order])->asArray()->one();
        $model2 = OrderDetails::find()->where(['id_order'=>$id_order])->asArray()->one();
        $model3 = Product::find()->where(['id_product'=>$model2['id_product']])->asArray()->one();
         if(count($model)!=0){ 
            $response = $model+$model2+$model3;
        }else{
            $response = [
                'status' => 'success',
                'message' => 'Order number not valid',
            ];
        }
        return $response;
    }

}