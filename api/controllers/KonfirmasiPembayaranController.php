<?php
namespace api\controllers;
use Yii;
use backend\models\KonfirmasiPembayaran;
use backend\models\Notifikasi;
use backend\models\Orders;

class KonfirmasiPembayaranController extends \yii\rest\Controller
{
	
	public $modelClass = 'backend\models\KonfirmasiPembayaran';

	protected function verbs()
	{
		return[
			'index' => ['POST'],
		];
	}

	public function actionIndex()
	{
		$id_order = !empty($_POST['id_order'])?$_POST['id_order']:'';
		$nama = !empty($_POST['nama'])?$_POST['nama']:'';
		$no_rek = !empty($_POST['no_rek'])?$_POST['no_rek']:'';
		$bank = !empty($_POST['bank'])?$_POST['bank']:'';
		$nominal = !empty($_POST['nominal'])?$_POST['nominal']:'';
		$tujuan = !empty($_POST['tujuan'])?$_POST['tujuan']:'';
		$tanggal = date('Y-m-d H:i:s');


        $model = new \backend\models\KonfirmasiPembayaran();
		$model->load(Yii::$app->getRequest()->getBodyParams(), '');

		if ($model->load(Yii::$app->getRequest()->getBodyParams(), '')) {
			$model->id_order = $id_order;
			$model->nama = $nama;
			$model->no_rek = $no_rek;
			$model->bank = $bank;
			$model->nominal_transfer = $nominal;
			$model->tujuan_transfer = $tujuan;
			$model->tanggal = $tanggal;
			$model->status = "Belum diverifikasi";
			$model->save();
			if($model->save()){
				$order = Orders::findOne($id_order);
				$order->status = "Verifikasi Pembayaran";
				$order->save();
				$notifikasi = new Notifikasi();
                $notifikasi->id_order = $id_order;
                $notifikasi->id_customer = $order->id_customer;
                $notifikasi->tanggal = date('Y-m-d H:i:s');
                $msg = "Pembayaran untuk order number : ".$id_order." sedang dalam proses verifikasi.";
                $notifikasi->pesan = $msg;
                $this->sendNotification($msg);
                    
                $notifikasi->save();
				$response = [
					"status" => "success"
				];

			}else{
				$response = [
					"status" => "error"
				];	
			}
				
			
		}

		return $response;
		
	}

	public function sendNotification( $message)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $msg = array
        (
            'body'  => "$message",
            'title'     => "Notifikasi"
            );
        $fields = array
        (
            'to'  => "/topics/customer",
            'notification'      => $msg
            );
        $headers = array(
            'Authorization:key = AAAAIzDq1fA:APA91bE8SLpph88dmlCQ5ZWAkdzVjtCamPAvyLqW1SkduuDdQwoT6dB6NitAT-w8rI_RRNqWke24WuvTOoVwdp1HLM_uYcdsqttn-kLbnwT7l0il7g_AU26SH3TRxFyREMzgRcSrQHly',
            'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);           
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }
}
?>