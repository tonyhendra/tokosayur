<?php
namespace api\controllers;
use Yii;
use backend\models\Kategori;

class KategoriController extends \yii\rest\Controller
{
	
	protected function verbs()
	{
		return[
			'index' => ['GET'],
		];
	}

	public function actionIndex()
	{
		$id_kategori = !empty($_GET['id_kategori'])?$_GET['id_kategori']:'';
		if(empty($id_kategori)){
			$models = Kategori::find()->asArray()->all();
			$response = [
				'data' => $models
			];
		}else{
			$models = Kategori::find()->where(['id_kategori'=>$id_kategori])->asArray()->all();
			$response = [
				'data' => $models
			];	
		}
		
		return $response;
	}
}
?>