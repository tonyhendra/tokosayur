<?php
namespace api\controllers;
use backend\models\Cart;
use backend\models\Product;
use Yii;

class CartController extends \yii\rest\Controller
{
	protected function verbs(){
		return [
			'index' => ['GET'],
			'add' => ['POST']
		];
	}

	public function actionIndex(){
		$username = Yii::$app->getRequest()->getQueryParam('username');
		$response = [];
		if(empty($username)){
			$response = [
				'status' => 'error',
				'message' => 'error',
			];
		}else{
			$cart = Cart::find()->where(['username' => $username ])->asArray()->all();
			//jika username ada maka
			// $data = [];
			// $data = $cart['Cart'];
			for($i=0; $i<count($cart);$i++){
				$product = Product::find()->where(['id_product' => $cart[$i]['id_product']])->asArray()->one();
				$cart[$i]['nama'] = $product['nama'];
				$cart[$i]['image'] = $product['image'];
				$cart[$i]['harga'] = $product['harga'];
			}
			if($cart!=null){

				$response = $cart;
				// $response = [
				// 	'status' => 'success',
				// 	'message' => '',
				// 	'data' => $cart
				// ];
			}else{
				$response = [
					'status' => 'success',
					'message' => 'Keranjang belanja anda kosong'

				];
			}

			


		}
		return $response;
	}

	public function actionAdd(){
		$response = [];
        $model = new Cart();
        $username = !empty($_POST['username'])?$_POST['username']:'';
		$id_product = !empty($_POST['id_product'])?$_POST['id_product']:'';
        $product = Cart::find()->where(['username'=>$username, 'id_product'=>$id_product])->one();

        if($product!=null){
        	$product->jumlah = $product->jumlah+1;
        	$product->save();
        	$response = [
	        		'status' => 'success',
	        		'message' => 'add cart berhasil update',
	        		//'data' => $user
	        ];
        }
        // load data dari POST request
        else{
        	$cart = $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        	if($cart){

	        	$model->save();
	        	$response = [
	        		'status' => 'success',
	        		'message' => 'add cart berhasil',
	        		//'data' => $user
	        	];
	        }else{
	        	$response = [
	        		'status' => 'error',
	        		'message' => 'add cart gagal',
	        	];
	        }

        }
        
      
        return $response;
        
	}


}