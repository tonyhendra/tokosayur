<?php
    namespace backend\assets;

    use yii\web\AssetBundle;

    class DatepickerAsset extends AssetBundle 
    {
        public $sourcePath = 'bower-asset/bootstrap-datepicker/dist';
        public $js = ['js/bootstrap-datepicker.min.js'];
        public $css = ['css/bootstrap-datepicker.min.css'];
    }