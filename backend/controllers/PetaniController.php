<?php

namespace backend\controllers;

use Yii;
use backend\models\Petani;
use backend\models\PetaniSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use backend\models\Pickupnode;

/**
 * PetaniController implements the CRUD actions for Petani model.
 */
class PetaniController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Petani models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new PetaniSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Petani model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Petani model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Petani();
        // $pickupnode = new Pickupnode();

        if ($model->load(Yii::$app->request->post())) {
            $model->save(); 
            // $pickup_id_petani = $model->id_petani;
            // if($pickupnode->load(Yii::$app->request->post())){
            //     $pickupnode->id_petani = $pickup_id_petani;
            //     $pickupnode->keterangan = "Petani : ".$model->nama;
            //     $pickupnode->save();
            // }
            return $this->redirect(['view', 'id' => $model->id_petani]);
        } else {
            return $this->render('create', [
                'model' => $model,
                //'pickupnode' => $pickupnode
            ]);
        }
    }

    /**
     * Updates an existing Petani model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        //$pickupnode = Pickupnode::findOne(['id_petani'=> $model->id_petani]);

        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            //$pickupnode->save();    
            return $this->redirect(['view', 'id' => $model->id_petani]);
        } else {
            return $this->render('update', [
                'model' => $model, 
                //'pickupnode' => $pickupnode
            ]);
        }
    }

    /**
     * Deletes an existing Petani model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        //Pickupnode::findOne(['id_petani' => $id])->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Petani model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Petani the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Petani::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
