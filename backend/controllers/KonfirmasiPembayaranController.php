<?php

namespace backend\controllers;

use Yii;
use backend\models\KonfirmasiPembayaran;
use backend\models\KonfirmasiPembayaranSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Orders;
use backend\models\Notifikasi;

/**
 * KonfirmasiPembayaranController implements the CRUD actions for KonfirmasiPembayaran model.
 */
class KonfirmasiPembayaranController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all KonfirmasiPembayaran models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new KonfirmasiPembayaranSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single KonfirmasiPembayaran model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new KonfirmasiPembayaran model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new KonfirmasiPembayaran();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing KonfirmasiPembayaran model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $order = Orders::find()->where(['id_order'=>$model->id_order])->one();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if($model->status=="Valid"){
                $order->status = "Pembayaran Diterima";
                $order->save();
                $notifikasi = new Notifikasi();
                $notifikasi->id_order = $order->id_order;
                $notifikasi->id_customer = $order->id_customer;
                $notifikasi->tanggal = date('Y-m-d H:i:s');
                $msg = "Pembayaran untuk order number : ".$order->id_order." telah kami terima.";
                $notifikasi->pesan = $msg;
                $this->sendNotification($msg);
                $notifikasi->save();
            }else if($model->status=="Tidak Valid"){
                $notifikasi = new Notifikasi();
                $notifikasi->id_order = $order->id_order;
                $notifikasi->id_customer = $order->id_customer;
                $notifikasi->tanggal = date('Y-m-d H:i:s');
                $msg = "Pembayaran untuk order number : ".$order->id_order." tidak valid.";
                $notifikasi->pesan = $msg;
                $this->sendNotification($msg);
                $notifikasi->save();
            }
            Yii::$app->session->setFlash('success', 'update successfully.');
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing KonfirmasiPembayaran model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the KonfirmasiPembayaran model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return KonfirmasiPembayaran the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = KonfirmasiPembayaran::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function sendNotification( $message)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $msg = array
        (
            'body'  => "$message",
            'title'     => "Notifikasi"
            );
        $fields = array
        (
            'to'  => "/topics/customer",
            'notification'      => $msg
            );
        $headers = array(
            'Authorization:key = AAAAIzDq1fA:APA91bE8SLpph88dmlCQ5ZWAkdzVjtCamPAvyLqW1SkduuDdQwoT6dB6NitAT-w8rI_RRNqWke24WuvTOoVwdp1HLM_uYcdsqttn-kLbnwT7l0il7g_AU26SH3TRxFyREMzgRcSrQHly',
            'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);           
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }
}
