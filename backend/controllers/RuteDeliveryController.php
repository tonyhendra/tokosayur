<?php

namespace backend\controllers;

use Yii;
use backend\models\RuteDelivery;
use backend\models\RuteDeliverySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\Orders;
use backend\models\Kendaraan;
/**
 * RuteDeliveryController implements the CRUD actions for RuteDelivery model.
 */

set_time_limit(1500);

class RuteDeliveryController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RuteDelivery models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RuteDeliverySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new RuteDelivery();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single RuteDelivery model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RuteDelivery model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RuteDelivery();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_rute]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing RuteDelivery model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_rute]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing RuteDelivery model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RuteDelivery model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RuteDelivery the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RuteDelivery::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionAlga(){
        $model = new RuteDelivery();
        //$model = [new RuteDelivery];
        $raw_data = Orders::find()->select(['id_order',])->where(['status' => 'Pembayaran Diterima'])->asArray()->all();

        $array_data = [];
        foreach ($raw_data as $val) {
           array_push($array_data, $val['id_order']);
        }

        if ($model->load(Yii::$app->request->post())) {
            $jumlah_kromosom = $model->jumlah_kromosom;
            $jumlah_generasi = $model->jumlah_generasi;
            $crossover_rate = $model->crossover_rate;
            $mutation_rate = $model->mutation_rate;
            $id_kendaraan = $model->id_kendaraan;
            $rand_kromosom = $model->randKromosom($array_data, $jumlah_kromosom, $id_kendaraan);
            $pop = [];
            $fitness_history = [];
            for ($i=0; $i < $jumlah_generasi ; $i++) { 
                if($i==0){
                    $pop = $model->alga($rand_kromosom, $crossover_rate, $mutation_rate);
                    $fitness = $model->fitness($pop);
                    array_push($fitness_history, $fitness[0]);
                }else{
                    $temp = $model->alga($pop, $crossover_rate, $mutation_rate);
                    $fitness = $model->fitness($temp);
                    array_push($fitness_history, $fitness[0]);
                    unset($pop);
                    $pop = $temp;
                }
            }

            $jarak = [];
            $jarak = $model->cariJarak($pop);
            $model->fitness_history = json_encode($fitness_history, JSON_NUMERIC_CHECK);
            $model->rute = json_encode($pop[0], JSON_NUMERIC_CHECK);
            $berat = 0;
            for ($i=0; $i < count($pop[0]); $i++) { 
                $berat = $berat+$model->getBerat($pop[0][$i]);
            }
            $model->berat = $berat;
            $model->total_jarak = $jarak[0];
            $model->status = 'Released';
            $model->tanggal = date('Y-m-d H:i:s');
            if($pop!=null){
                for($i=0;$i<count($pop[0]);$i++){
                    $modelOrder = Orders::find()->where(['id_order' => $pop[0][$i] ])->one();
                    //$modelOrder->status = "Pesanan Dikirim";
                    $modelKendaraan = Kendaraan::find()->where(['id_kendaraan' => $id_kendaraan])->one();
                    //$modelKendaraan->status = "in Use";
                    $modelOrder->save();
                }
                $model->save(); 
                return $this->render('view',['id' => $model->id_rute, 'model'=>$model]);
            
            }

        }else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }



    }
}
