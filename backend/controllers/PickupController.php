<?php

namespace backend\controllers;

use Yii;
use backend\models\Supply;
use backend\models\SupplySearch;
use backend\models\Petani;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

set_time_limit(500);
class PickupController extends Controller
{
	public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

     /**
     * Lists all models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new RutePickup();

        return $this->render('index', ['model' => $model]);
    }

    public function actionGet()
    {
        $request = Yii::$app->request;
        $obj = $request->post('obj');
        $value = $request->post('value');
        $data = Kendaraan::find()->where([$obj => $value])->all();
        $tagOptions = ['prompt' => "=== Select ==="];
        return Html::renderSelectOptions([], ArrayHelper::map($data, 'id', 'name'), $tagOptions);
    }

    public function actionAlga()
    {
        $model = new RutePickup();
        $raw_data = Yii::app()->db->createCommand('SELECT * FROM supply where status="Released')->queryAll();
        $array_data = array();
        foreach($raw_data as $val)
        {
            $array_data[$val->id] = $val->attributes;
        }
        $rand_kromosom = initPopulation($array_data, $jumlah_kromosom);
        $pop = [];
        for ($i=0; $i < $model->jumlah_generasi ; $i++) { 
            # code...
            if($i==0){
                $pop = $model->alga($rand_kromosom, $model->jumlah_kromosom, $model->crossover_rate, $model->mutation_rate);
            }else{
                $temp = $model->alga($pop, $model->jumlah_kromosom, $model->crossover_rate, $model->mutation_rate);
                unset($pop);
                $pop = $temp;
            }
        }
        $model->berat = 0;
        $rute = json_encode($pop[0]);
        $model->rute = $rute;
        $model->total_jarak = 0;
        $model->status = 'Released';
        if ($model->load(Yii::$app->request->post())) {
            $model->save(); 
            return $this->redirect(['view', 'id' => $model->id_rute]);
        } else {
            return $this->render('index', [
                'model' => $model,
                //'pickupnode' => $pickupnode
            ]);
        }

    }


    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        //Pickupnode::findOne(['id_petani' => $id])->delete();

        return $this->redirect(['index']);
    }

    protected function findModel($id)
    {
        if (($model = RutePickup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}