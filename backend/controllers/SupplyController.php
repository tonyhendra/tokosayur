<?php

namespace backend\controllers;

use Yii;
use backend\models\Supply;
use backend\models\SupplySearch;
use backend\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SupplyController implements the CRUD actions for Supply model.
 */
class SupplyController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Supply models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SupplySearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Supply model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Supply model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Supply();        
        if ($model->load(Yii::$app->request->post())) {
            $berat = $model->getProductBerat($model->id_product);
            $model->berat = $berat;
            $total_berat = $model->jumlah*$berat;
            $model->total_berat = $total_berat/1000;
            $model->save();
            return $this->redirect(['view', 'id' => $model->id_supply]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Supply model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $product = Product::findOne(['id_product' => $model->id_product]);

        if ($model->load(Yii::$app->request->post())){
            $status = $model->status;
            $berat = $model->getProductBerat($model->id_product);
            $model->berat = $berat;
            $total_berat = $model->jumlah*$berat;
            $jumlah = $model->jumlah;
            $model->total_berat = $total_berat/1000;
            $model->save();
            if($status == "Completed"){
                $product->stock = $product->stock+$jumlah;
                $product->save();
            }
            return $this->redirect(['view', 'id' => $model->id_supply]);
        } else {
            return $this->render('update', [
                'model' => $model,'product' => $product
            ]);
        }
    }

    /**
     * Deletes an existing Supply model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Supply model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Supply the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Supply::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
