<?php

namespace backend\controllers;

use Yii;
use backend\models\Orders;
use backend\models\OrdersSearch;
use backend\models\OrderDetails;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use backend\models\DynamicForms;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;
use backend\models\Product;
use backend\models\Notifikasi;
/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Orders models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrdersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        // $dataProvider = new \yii\data\ArrayDataProvider([
        //     'key'=>'id_order',
        //     'allModels' => $dataProvider_,
        //     'sort' => [
        //         'attributes' => ['id_order' => SORT_DESC],
        //     ],
        // ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $model2 = OrderDetails::find()->where(['id_order'=> $id]);
        $dataProvider = new ActiveDataProvider(['query' => $model2, 'sort'=> false]);
        // $hitung = OrderDetails::find()->where(['id_order'=> $id])->count();
        return $this->render('view', [
            'model' => $model, 'model2' => $dataProvider,
        ]);
    }

    /**
     * Creates a new Orders model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Orders();
        $modelDetails = [new OrderDetails];

        $error = 0;

        if ($model->load(Yii::$app->request->post())) {

            $modelDetails = DynamicForms::createMultiple(OrderDetails::className());
            DynamicForms::loadMultiple($modelDetails, Yii::$app->request->post());

            $valid = $model->validate();
            $valid = DynamicForms::validateMultiple($modelDetails) && $valid;

            // $data=Yii::$app->request->post('Orders');
            // $this->simpan($data,$model->id_order);
            $total_harga = 0;
            $total_berat = 0;
            foreach ($modelDetails as $val) {
                $harga = $val->jumlah * $val->getHarga($val->id_product);
                $berat = $val->jumlah * $val->getBerat($val->id_product);
                $total_harga = $total_harga+$harga;
                $total_berat = $total_berat+$berat;
            }
            $model->total_harga = $total_harga;
            $model->total_berat = $total_berat/1000;
            if($valid){
                foreach ($modelDetails as $val) {
                    $product = Product::findOne($val->id_product);
                    $product->stock = $product->stock-$val->jumlah;
                    $product->save();
                }
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($flag = $model->save(false)){

                        foreach ($modelDetails as $val) {
                            # code...
                            $val->id_order = $model->id_order;
                            //$harga = $val->getHarga($val->id_product);
                            //$total_harga = $total_harga+$harga;
                            if(!$flag = $val->save(false)){
                                $transaction->rollBack();
                                break;
                            }
                        }
                        

                    }
                    if($flag){
                        $transaction->commit();
                        return $this->redirect(['view', 'id' => $model->id_order]);
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                }
            }
            else{

                return $this->render('create', [
                    'model' => $model, 
                    'modelDetails' => (empty($modelDetails)) ? [new OrderDetails] : $modelDetails,
                    'error' => print_r($modelDetails,true)
                ]);

            }
            
        }
        else{

            return $this->render('create', [
                'model' => $model, 
                'modelDetails' => (empty($modelDetails)) ? [new OrderDetails] : $modelDetails,
                'error' => $error
            ]);
            //return $this->redirect(['test']);

        }
        
    }

    /**
     * Updates an existing Orders model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $modelDetails = $model->orderDetails;
        $status_old = $model->status;

        if ($model->load(Yii::$app->request->post())) {
            $oldIDs = ArrayHelper::map($modelDetails, 'id', 'id');
            $modelDetails = DynamicForms::createMultiple(OrderDetails::classname(), $modelDetails);
            DynamicForms::loadMultiple($modelDetails, Yii::$app->request->post());
            $deletedIDs = array_diff($oldIDs, array_filter(ArrayHelper::map($modelDetails, 'id', 'id')));
            
            $valid = $model->validate();
            $valid = DynamicForms::validateMultiple($modelDetails) && $valid;

            // $data=Yii::$app->request->post('Orders');
            // $this->simpan($data,$model->id_order);
            $total_harga = 0;
            $total_berat = 0;
            $status = $model->status;
            foreach ($modelDetails as $val) {
                $harga = $val->jumlah * $val->getHarga($val->id_product);
                $berat = $val->jumlah * $val->getBerat($val->id_product);
                $total_harga = $total_harga+$harga;
                $total_berat = $total_berat+$berat;
            }
            $model->total_harga = $total_harga;
            $model->total_berat = $total_berat/1000;

            if($valid){
                if($status_old!=$status){
                    $notifikasi = new Notifikasi();
                    $notifikasi->id_order = $model->id_order;
                    $notifikasi->id_customer = $model->id_customer;
                    $notifikasi->tanggal = date('Y-m-d H:i:s');
                    if($status == "Dibatalkan"){
                        foreach ($modelDetails as $val) {
                            $product = Product::findOne($val->id_product);
                            $product->stock = $product->stock+$val->jumlah;
                            $product->save();
                        }
                    }
                    if($status == "Pembayaran Diterima"){
                        $msg = "Pembayaran untuk order number : ".$model->id_order." telah kami terima.";
                        $notifikasi->pesan = $msg;
                        $this->sendNotification($msg);
                    }else{
                        $msg = "Pesanan anda dengan order number : ".$model->id_order." telah berganti status menjadi ".$model->status;
                        $notifikasi->pesan = $msg;
                        $this->sendNotification($msg);

                    }
                    
                    $notifikasi->save();
                }
                $transaction = \Yii::$app->db->beginTransaction();
                try{
                    if($flag = $model->save(false)){
                        if(!empty($deletedIDs)){
                            OrderDetails::deleteAll(['id'=> $deletedIDs]);
                        }

                        foreach ($modelDetails as $val) {
                            # code...
                            $val->id_order = $model->id_order;
                            //$harga = $val->getHarga($val->id_product);
                            //$total_harga = $total_harga+$harga;
                            if(!$flag = $val->save(false)){
                                $transaction->rollBack();
                                break;
                            }
                        }
                        

                    }
                    if($flag){
                        $transaction->commit();
                        if($status == "Completed"){
                            foreach ($modelDetails as $val) {
                                $jml = $val->jumlah;
                                $product = Product::findOne(['id_product' => $val->id_product]);
                                $product->stock = $product->stock-$jml;
                                $product->save();
                            }
                            
                        }
                        Yii::$app->session->setFlash('success', 'update successfully.');
                        return $this->redirect(['view', 'id' => $model->id_order]);
                    }
                }catch(Exception $e){
                    $transaction->rollBack();
                }

            }
        }

        return $this->render('update', [
            'model' => $model, 'modelDetails' => $modelDetails
        ]);
    }

    /**
     * Deletes an existing Orders model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        
        if ($model->delete()) {
            Yii::$app->session->setFlash('success', 'deleted successfully.');
 
        }
        return $this->redirect(['index']);
    }

    /**
     * Finds the Orders model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Orders the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Orders::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    // public function simpan($data, $id_order)
    // {
    //     $id_product=$data['id_product'];
    //     $jumlah = $data['jumlah'];
    //     foreach ($id_product as $id)
    //     {
    //         $OrderDetails = new OrderDetails();
    //         $OrderDetails->id_order=$id_order;
    //         $OrderDetails->id_pegawai=$id_product;
    //         $OrderDetails->jumlah = $jumlah;
    //         $OrderDetails->save();
    //     }
    // }
    public function sendNotification( $message)
    {
        $url = 'https://fcm.googleapis.com/fcm/send';

        $msg = array
        (
            'body'  => "$message",
            'title'     => "Notifikasi"
            );
        $fields = array
        (
            'to'  => "/topics/customer",
            'notification'      => $msg
            );
        $headers = array(
            'Authorization:key = AAAAIzDq1fA:APA91bE8SLpph88dmlCQ5ZWAkdzVjtCamPAvyLqW1SkduuDdQwoT6dB6NitAT-w8rI_RRNqWke24WuvTOoVwdp1HLM_uYcdsqttn-kLbnwT7l0il7g_AU26SH3TRxFyREMzgRcSrQHly',
            'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);  
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);           
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
        curl_close($ch);
    }

}
