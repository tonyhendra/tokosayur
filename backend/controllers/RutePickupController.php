<?php

namespace backend\controllers;

use Yii;
use backend\models\RutePickup;
use backend\models\RutePickupSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use backend\models\Supply;

/**
 * RutePickupController implements the CRUD actions for RutePickup model.
 */
set_time_limit(1500);
class RutePickupController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all RutePickup models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new RutePickupSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $model = new RutePickup();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider, 
            'model' => $model
        ]);
    }

    /**
     * Displays a single RutePickup model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RutePickup model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RutePickup();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_rute]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RutePickup model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_rute]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RutePickup model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RutePickup model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RutePickup the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RutePickup::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionAlga()
    {
        $model = new RutePickup();
        $raw_data = Supply::find()->select(['id_supply','id_petani',])->where(['status' => 'Released'])->asArray()->all();

        $array_data = [];
        foreach ($raw_data as $val) {
           array_push($array_data, $val['id_petani']);
        }

        $array_id_supply = [];
        foreach ($raw_data as $val) {
           array_push($array_id_supply, $val['id_supply']);
        }
        if ($model->load(Yii::$app->request->post())) {
            $jumlah_kromosom = $model->jumlah_kromosom;
            $jumlah_generasi = $model->jumlah_generasi;
            $crossover_rate = $model->crossover_rate;
            $mutation_rate = $model->mutation_rate;
            $id_kendaraan = $model->id_kendaraan;
            $rand_kromosom = $model->randKromosom($array_data, $jumlah_kromosom, $array_id_supply, $id_kendaraan);
            $pop = [];
            $fitness_history = [];
            for ($i=0; $i < $jumlah_generasi ; $i++) { 
                if($i==0){
                    $pop = $model->alga($rand_kromosom, $crossover_rate, $mutation_rate);
                    $fitness = $model->fitness($pop);
                    array_push($fitness_history, $fitness[0]);
                }else{
                    $temp = $model->alga($pop, $crossover_rate, $mutation_rate);
                    $fitness = $model->fitness($temp);
                    array_push($fitness_history, $fitness[0]);
                    unset($pop);
                    $pop = $temp;
                }
            }

            $jarak = [];
            $jarak = $model->cariJarak($pop);
            $model->fitness_history = json_encode($fitness_history, JSON_NUMERIC_CHECK);
            $model->rute = json_encode($pop[0], JSON_NUMERIC_CHECK);
            $berat = 0;
            for ($i=0; $i < count($pop[0]); $i++) { 
                # code...
                for($j=0; $j < count($array_id_supply); $j++){
                    $berat = $berat+$model->getBerat($pop[0][$i],$array_id_supply[$j]);
                }
                
            }
            $model->berat = $berat;
            $model->total_jarak = $jarak[0];
            $model->status = 'Released';
            if($pop!=null){
                $model->save(); 
                return $this->render('view',['id' => $model->id_rute, 'model'=>$model]);
            
            }

        }else {
            return $this->render('index', [
                'model' => $model,
            ]);
        }


        

    }

    public function actionTest(){

        $model = new RutePickup();
        $raw_data = Supply::find()->select(['id_petani',])->where(['status' => 'Released'])->asArray()->all();

        // foreach($raw_data as $val)
        // {
        //     $array_data[$val->id] = $val->attributes;
        // }
        $array_data = [];
        foreach ($raw_data as $val) {
           array_push($array_data, $val['id_petani']);
        }
        $jumlah_kromosom = 5;
        $jumlah_generasi = 100;
        $crossover_rate = 0.75;
        $mutation_rate = 0.25;
        $rand_kromosom = RutePickup::randKromosom($array_data, $jumlah_kromosom);
        $pop = [];
        for ($i=0; $i < $jumlah_generasi ; $i++) { 
            if($i==0){
                $pop = $model->alga($rand_kromosom, $jumlah_kromosom, $crossover_rate, $mutation_rate);
            }else{
                $temp = $model->alga($pop, $jumlah_kromosom, $crossover_rate, $mutation_rate);
                unset($pop);
                $pop = $temp;
            }
        }
        $rute = $pop[0];
        $jarak = [];
        $jarak = $model->cariJarak($pop);

        return $this->render('test',['model' => $array_data,'jml'=>$pop,]);

    }
}
