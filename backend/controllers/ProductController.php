<?php

namespace backend\controllers;

use Yii;
use backend\models\Product;
use backend\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Product();
        $model->terjual = 0;
        if ($model->load(Yii::$app->request->post())) {
            try{
                $flag = UploadedFile::getInstance($model,'image');
                if(!empty($flag) && $flag->size != 0){
                    $model->image = $model->id_product.'_'.$model->nama.'.'.$flag->extension;
                }else{
                    $imgstats = false;
                }

                if($model->save()){
                    if($imgstats){
                        $flag->saveAs('uploads/'.$model->id_product.'_'.$model->nama.'.'.$flag->extension);    
                    }
                    
                    Yii::$app->getSession()->setFlash('succcess','Data Saved');
                    return $this->redirect(['view', 'id' => $model->id_product]);

                }else{
                    Yii::$app->getSession()->setFlash('error','Data not saved!');
                    return $this->render('create', [
                         'model' => $model,
                         ]);
                }

            }catch(Exception $e){
              Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
            }
          }else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $current_image = $model->image; 
        if ($model->load(Yii::$app->request->post())) {
         try{
                $flag = UploadedFile::getInstance($model,'image');
                if(!empty($flag) && $flag->size != 0){
                    if ($current_image!=$flag->name){
                        if(!empty($current_image) ){
                            unlink(Yii::getAlias('@backend').'/web/uploads/'.$current_image);
                        }
                         $model->image = $model->id_product.'_'.$model->nama.'.'.$flag->extension;
                        $flag->saveAs('uploads/'.$model->id_product.'_'.$model->nama.'.'.$flag->extension);
                        $model->save();
                        Yii::$app->getSession()->setFlash('succcess','Data Saved');
                        return $this->redirect(['view', 'id' => $model->id_product]);
                    }else{
                        $model->save();
                        Yii::$app->getSession()->setFlash('succcess','Data Saved');
                        return $this->redirect(['view', 'id' => $model->id_product]);
                    }
                   
                }else{
                    //Yii::$app->getSession()->setFlash('error','Data not saved!');
                    // $this->imageDelete($id);
                    // $model->image = "";
                    // $model->save();
                    $model->save();
                    Yii::$app->getSession()->setFlash('succcess','Data Saved');
                    return $this->redirect(['view', 'id' => $model->id_product]);
                }

            }catch(Exception $e){
              Yii::$app->getSession()->setFlash('error',"{$e->getMessage()}");
            }
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        if($model->image!=""){
            $flag = Yii::getAlias('@backend').'/web/uploads/'.$model->image;
            if(file_exists($flag)){
                unlink($flag);
            }
        }
        $model->delete();
        Yii::$app->getSession()->setFlash('success',"Deleted successfully");

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function imageDelete($id){
        $model = $this->findModel($id);
        if($model->image!=""){
            $flag = Yii::getAlias('@backend').'/web/uploads/'.$model->image;
            if(file_exists($flag)){
                unlink($flag);
            }
        }
    }

}
