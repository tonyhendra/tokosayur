<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\file\FileInput;


?>

<div class="pickup-form">
    <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Product</h3>
            </div>
            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'deskripsi')->textarea(['rows' => 6]) ?>
                
                <?= $form->field($model, 'berat')->textInput()->hint("Satuan dalam gram") ?>

                <?= $form->field($model, 'harga')->textInput() ?>

                <?= $form->field($model, 'stock')->textInput() ?>

                <?= $form->field($model, 'id_kategori')->dropDownList(ArrayHelper::map(Kategori::find()->all(), 'id_kategori', 'nama_kategori'))->label('Kategori') ?>
                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions'=>['allowedFileExtensions'=>['jpg','gif','png'],
                    'showPreview' => true,
                    'showCaption' => true,
                    $model->isNewRecord ? '' :
                    'initialPreview'=> [
                        '<img src="'.Yii::$app->request->baseUrl.'/uploads/'.$model->image.'" width="100%" height="100%" class="file-preview-image">',
                    ],
                    'overwriteInitial'=>true,
                    ],
                    ]);   
                ?>
          </div>
      </div>
    </div>
  </section>
</div>
            
            
            