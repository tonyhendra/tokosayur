<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\KonfirmasiPembayaran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="konfirmasi-pembayaran-form">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $model->isNewRecord ? 'Create' : 'Update'?> Konfirmasi Pembayaran</h3>
                    </div>
                    <!-- /.box-header -->
                    <!-- form start -->

                    <?php $form = ActiveForm::begin(); ?>
                    <div class="box-body">

                        <?= $form->field($model, 'id_order')->textInput(['readonly' => true]) ?>

                        <?= $form->field($model, 'nama')->textInput(['maxlength' => true, 'readonly' => true]) ?>

                        <?= $form->field($model, 'no_rek')->textInput(['maxlength' => true, 'readonly' => true]) ?>

                        <?= $form->field($model, 'bank')->textInput(['maxlength' => true, 'readonly' => true]) ?>

                        <?= $form->field($model, 'nominal_transfer')->textInput(['maxlength' => true, 'readonly' => true]) ?>

                        <?= $form->field($model, 'tujuan_transfer')->textInput(['maxlength' => true, 'readonly' => true]) ?>

                        <?= $form->field($model, 'tanggal')->textInput(['readonly' => true]) ?>

                        <!-- <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?> -->
                        <?= $form->field($model, 'status')->dropDownList(['Belum diverifikasi' => 'Belum diverifikasi', 'Valid' => 'Valid', 'Tidak Valid' => 'Tidak Valid']);?>
                        <div class="box-footer">
                            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                        </div>

                        <?php ActiveForm::end(); ?>
                    </div>

                </div>
            </div>
        </div>
    </section>
</div>
