<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\KonfirmasiPembayaranSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Konfirmasi Pembayarans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfirmasi-pembayaran-index">
 <div class="box">
    <div class="box-header">
        <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        <!-- <p> -->
         <?= Html::a('Create Konfirmasi Pembayaran', ['create'], ['class' => 'btn btn-success']) ?>
        <!-- </p> -->
    </div>
    <div class="box-body">
    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'id_order',
            'nama',
            'no_rek',
            'bank',
            'nominal_transfer',
            'tujuan_transfer',
            'tanggal',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
</div>
</div>
