<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\KonfirmasiPembayaran */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Konfirmasi Pembayarans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="konfirmasi-pembayaran-view">
    <section class="content">
            <div class="row">
                <div class="col-md-8">
                    <div class="box">
                        <div class="box-header">
                            <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Delete', ['delete', 'id' => $model->id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id',
                            'id_order',
                            'nama',
                            'no_rek',
                            'bank',
                            'nominal_transfer',
                            'tujuan_transfer',
                            'tanggal',
                            'status',
                        ],
                    ]) ?>

                    </div>
                </div>
            </div>
        </div>
    </section>

</div>

