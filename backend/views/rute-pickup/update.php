<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RutePickup */

$this->title = 'Update Rute Pickup: ' . $model->id_rute;
$this->params['breadcrumbs'][] = ['label' => 'Rute Pickups', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rute, 'url' => ['view', 'id' => $model->id_rute]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rute-pickup-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
