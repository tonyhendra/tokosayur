<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use miloschuman\highcharts\Highcharts;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polyline;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use dosamigos\google\maps\Point;
use dosamigos\google\maps\Size;
use dosamigos\google\maps\overlays\Icon;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\RutePickup */

$this->title = $model->id_rute;
$this->params['breadcrumbs'][] = ['label' => 'Rute Pickups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rute-pickup-view">
<section class="content">
      <div class="row">
        <div class="col-md-12">
        <div class="box">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id_rute], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id_rute], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <!-- /.box-header -->
            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id_rute',
                    ['label' => 'Kendaraan',
                        'value' => function($data){return $data->kendaraan->jenis_kendaraan;}],
                    ['label' => 'Driver',
                        'value' => function($data){return $data->driver->username;}],
                    'jumlah_kromosom',
                    'jumlah_generasi',
                    'berat',
                    'rute',
                    'total_jarak',
                    //'fitness_history',
                    'status',
                ],
            ]) ?>

        <br>
        
            <div style="width: 98%;">

            
                    <div class="row bs-wizard" style="border-bottom:0;">
                
                        <div class="col-xs-3 bs-wizard-step complete">
                          <div class="text-center bs-wizard-stepnum">Start</div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Depot</div>
                        </div>
                        <?php

                            $rute = json_decode($model->rute);
                            for($i=0; $i<count($rute);$i++){
                                echo "
                                <div class='col-xs-3 bs-wizard-step complete'>
                                  <div class='text-center bs-wizard-stepnum'>".$rute[$i]."</div>
                                  <div class='progress'><div class='progress-bar'></div></div>
                                  <a href='#' class='bs-wizard-dot'></a>
                                  <div class='bs-wizard-info text-center'>".$model->getAlamat($rute[$i])."</div>
                                </div>
                                ";

                            }


                        ?>
                        
                        <div class="col-xs-3 bs-wizard-step complete"><!-- active -->
                          <div class="text-center bs-wizard-stepnum">Finish</div>
                          <div class="progress"><div class="progress-bar"></div></div>
                          <a href="#" class="bs-wizard-dot"></a>
                          <div class="bs-wizard-info text-center">Depot</div>
                        </div>
                    </div>
                    <br>
            <?php

            echo Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Fitness History'],
                    'xAxis' => [
                        'tickInterval' => 1,
                        'title' => ['text' => 'Generasi',]
                    ],

                    'yAxis' => [
                        'type' => 'logarithmic',
                        'minorTickInterval' => 0.000000000000001
                    ],
                    'tooltip' => [
                        'headerFormat' => '<b>{series.name}</b><br />',
                        'pointFormat' => 'Generasi ke - {point.x}, fitness = {point.y}'
                    ],
                    // 'plotOptions' => [
                    //     'line' => [
                    //         'dataLabels' => [
                    //             'enabled' => true
                    //         ],
                    //         'enableMouseTracking' => false
                    //     ]
                    // ],

                    'series' => [['name' => 'Nilai Fitness',
                        'data' => json_decode($model->fitness_history),
                        'pointStart' => 1
                    ]]
                ]
            ]);
            ?>
        </div>
        <?php
            $center = new LatLng(['lat' => -7.275973, 'lng' => 112.808304]);
            $map = new Map([
            'center' => $center,
            'zoom' => 11,
            'width' => '100%',
            'height' => 400,
            'containerOptions' => [
            'id' => 'artworkItemsMap'
            ]
            ]);
            $rute = json_decode($model->rute);
            $icon = new Icon([
                'url'=> "https://www.brokefordwich.com.au/wp-content/uploads/2015/05/google-maps.png", // url
                'scaledSize' => new Size([
                    "width" => 50,
                    "height" => 50
                    ]), // scaled size
                //'origin' => new Point([ 'x' => 1 , 'y' => 1]), // origin
                //'anchor'=> new Point(['x' => 25, 'y' => 0.1]) // anchor

            ]);
            $coordDepot = new LatLng(['lat' =>$model->getLatDepot(), 'lng' => $model->getLngDepot()]);
            $markerDepot = new Marker([
                'position' => $coordDepot,
                'icon' => $icon,
                ]);
            $markerDepot->attachInfoWindow(
                    new InfoWindow([
                    'content' => Html::tag('p', "Depot : ".$model->getAlamatDepot(),['class'=>'description-block'])
                    ])
                );
            $map->addOverlay($markerDepot);
            $coords = [];
            for($i=0; $i<count($rute);$i++){
                // Add markers
                $coord = new LatLng(['lat' =>$model->getLat($rute[$i]), 'lng' => $model->getLng($rute[$i])]);
                array_push($coords, $coord);
                $marker = new Marker([
                'position' => $coord,
                ]);

                // Adding InfoWindow to the marker
                $marker->attachInfoWindow(
                    new InfoWindow([
                    'content' => Html::tag('p', $rute[$i]." : ".$model->getAlamat($rute[$i]),['class'=>'description-block'])
                    ])
                );

                $map->addOverlay($marker);
                //if($i<(count($rute)-1)){
                if($i==0){
                    $map->appendScript(
                        '
                        var line = new google.maps.Polyline({
                            path: [
                                new google.maps.LatLng('.$model->getLatDepot().','.$model->getLngDepot().'),
                                new google.maps.LatLng('.$model->getLat($rute[$i]).','.$model->getLng($rute[$i]).'),
                                new google.maps.LatLng('.$model->getLat($rute[$i+1]).','.$model->getLng($rute[$i+1]).')
                            ],
                            strokeColor: "#FF0000",
                            strokeOpacity: 1.0,
                            strokeWeight: 3,
                        });                      

                        line.setMap(gmap0);
                        '
                    );
                }else if($i==(count($rute)-1)){
                    $map->appendScript(
                        '
                        var line = new google.maps.Polyline({
                            path: [
                                new google.maps.LatLng('.$model->getLat($rute[$i]).','.$model->getLng($rute[$i]).'),
                                new google.maps.LatLng('.$model->getLatDepot().','.$model->getLngDepot().'),
                            ],
                            strokeColor: "#FF0000",
                            strokeOpacity: 1.0,
                            strokeWeight: 3,
                        });                      

                        line.setMap(gmap0);
                        '
                    );
                }else{
                    $map->appendScript(
                        '
                        var line = new google.maps.Polyline({
                            path: [
                                new google.maps.LatLng('.$model->getLat($rute[$i]).','.$model->getLng($rute[$i]).'),
                                new google.maps.LatLng('.$model->getLat($rute[$i+1]).','.$model->getLng($rute[$i+1]).')
                            ],
                            strokeColor: "#FF0000",
                            strokeOpacity: 1.0,
                            strokeWeight: 3,
                        });                      

                        line.setMap(gmap0);
                        '
                    );
                }
                    
                //}
                

                // $waypoints = [
                // new DirectionsWayPoint(['location' => $rute[count($rute)-1]])
                // ];

                // $directionsRequest = new DirectionsRequest([
                //     'origin' => new LatLng(['lat' =>$model->getLat($rute[$i]), 'lng' => $model->getLng($rute[$i])]),
                //     'destination' => new LatLng(['lat' =>$model->getLat($rute[$i+1]), 'lng' => $model->getLng($rute[$i+1])]),
                //     'waypoints' => $waypoints,
                //     'travelMode' => TravelMode::DRIVING
                // ]);

                // $polyline = new Polyline([
                // 'path' => $coord,
                // ]);
                // $polylineOptions = new PolylineOptions([
                //     'strokeColor' => '#FFAA00',
                //     'draggable' => true
                // ]);
                // $directionsRenderer = new DirectionsRenderer([
                //     'map' => $map->getName(),
                //     'polylineOptions' => $polylineOptions
                // ]);
                // // Finally the directions service
                // $directionsService = new DirectionsService([
                //     'directionsRenderer' => $directionsRenderer,
                //     'directionsRequest' => $directionsRequest
                // ]);

                // // Thats it, append the resulting script to the map
                // $map->appendScript($directionsService->getJs());
                // $map->addOverlay($polyline);

            }
            //$boundsCenter = LatLngBounds::getBoundsOfMarkers($map->getMarkers())->getCenterCoordinates();
            //$map->setCenter($boundsCenter); 
            

            echo $map->display();
        ?>
    </div>
    </div>
</div>
</section>

</div>
