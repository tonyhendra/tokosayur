<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RutePickup */

$this->title = 'Create Rute Pickup';
$this->params['breadcrumbs'][] = ['label' => 'Rute Pickups', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rute-pickup-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
