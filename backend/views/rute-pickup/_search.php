<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RutePickupSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-pickup-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rute') ?>

    <?= $form->field($model, 'id_kendaraan') ?>

    <?= $form->field($model, 'id_driver') ?>

    <?= $form->field($model, 'berat') ?>

    <?= $form->field($model, 'rute') ?>

    <?php // echo $form->field($model, 'total_jarak') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
