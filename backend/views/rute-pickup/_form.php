<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RutePickup */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-pickup-form">
     <section class="content">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Rute</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

            <?= $form->field($model, 'id_kendaraan')->textInput() ?>

            <?= $form->field($model, 'id_driver')->textInput() ?>

            <?= $form->field($model, 'berat')->textInput() ?>

            <?= $form->field($model, 'rute')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'total_jarak')->textInput() ?>

            <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

            <div class="form-group">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
</div>
</section>

</div>
