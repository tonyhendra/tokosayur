<?php

use yii\helpers\Html;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductSeacrh */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
     <div class="box">
            <div class="box-header">
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <div class="box-body">
    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'id_product',
            'nama',
            //'deskripsi:ntext',
             [
               'label' => 'Berat (gram)',
               'value' => function ($model) {
                   return $model->berat;
               },
             ],
            'harga',
            'stock',
            'terjual',
            ['label'=>'Kategori',
            'value' => function($data){return $data->kategori->nama_kategori;}],
            // 'id_kategori',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
