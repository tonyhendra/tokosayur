<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <section class="content">
      <div class="row">
        <div class="col-md-8">
         <div class="box">

        <div class="box">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id_product], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id_product], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

            <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'id_product',
                    'nama',
                    'deskripsi:ntext',
                    'berat',
                    'harga',
                    'stock',
                    'terjual',
                    'id_kategori',
                    ['label'=>'Kategori',
                    'value' => function($data){return $data->kategori->nama_kategori;}],
                     [
                         'label'=>'Image',
                         'format'=>'raw',
                         'value'=>Html::img(Yii::$app->request->baseUrl.'/uploads/'.$model->image,['width'=>'100px']),
                   ],
                ],
            ]) ?>
            </div>
        </div>
    </div>
</div>
</div>
</section>

</div>
