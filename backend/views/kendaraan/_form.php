<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use common\models\User;
use backend\models\Petani;
/* @var $this yii\web\View */
/* @var $model app\models\Kendaraan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kendaraan-form">
	<section class="content">
	    <div class="row">
	        <!-- left column -->
	        <div class="col-md-6">
	          <!-- general form elements -->
	          	<div class="box box-primary">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Kendaraan</h3>
	            	</div>
	            	<!-- /.box-header -->
	            	<!-- form start -->

    				<?php $form = ActiveForm::begin(); ?>
    				<div class="box-body">
				    <?= $form->field($model, 'jenis_kendaraan')->textInput(['maxlength' => true]) ?>
				    <?= $form->field($model, 'kapasitas')->textInput()->hint("Dalam satuan Kg") ?>
				    <?= $form->field($model, 'nomor_kendaraan')->textInput(['maxlength' => true]) ?>
				    <?= $form->field($model, 'id_driver')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(User::find()->where('role = 2')->all(), 'id', 'username'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select Driver ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        ])->label('Driver'); ?>
                    <?= $form->field($model, 'status')->dropDownList(['Idle' => 'Idle', 'In Use' => 'In Use', 'Maintenance' => 'Maintenance']);?>
				    <div class="box-footer">
				        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
				    </div>
				    <?php ActiveForm::end(); ?>
				</div>
			</div>
		</div>
    </section>

</div>
