<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KendaraanSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kendaraans';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kendaraan-index">
    <div class="box">
        <div class="box-header">
            <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
            <!-- <p> -->
             <?= Html::a('Create Kendaraan', ['create'], ['class' => 'btn btn-success']) ?>
            <!-- </p> -->
        </div>
        <?= DataTables::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id_kendaraan',
                'jenis_kendaraan',
                ['label'=>'Kapasitas (Kg)',
                    'value' => function($data){return $data->kapasitas;}],
                'nomor_kendaraan',
                ['label'=>'Driver',
                    'value' => function($data){return $data->driver->username;}],
                'status',

                ['class' => 'yii\grid\ActionColumn'],

            ],
        ]); ?>
    </div>
</div>
