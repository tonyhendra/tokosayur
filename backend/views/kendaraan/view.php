<?php

use yii\helpers\Html;
use yii\widgets\DetailView;


/* @var $this yii\web\View */
/* @var $model app\models\Kendaraan */

$this->title = $model->id_kendaraan;
$this->params['breadcrumbs'][] = ['label' => 'Kendaraans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kendaraan-view">
    <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header">
                        <?= Html::a('Update', ['update', 'id' => $model->id_kendaraan], ['class' => 'btn btn-primary']) ?>
                        <?= Html::a('Delete', ['delete', 'id' => $model->id_kendaraan], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">

                    <?= DetailView::widget([
                        'model' => $model,
                        'attributes' => [
                            'id_kendaraan',
                            'jenis_kendaraan',
                            'kapasitas',
                            'nomor_kendaraan',
                            'id_driver',
                            'status',
                        ],
                    ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
