<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\datepicker\DatePicker;
use backend\assets\DatepickerAsset;


/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = 'Create User';
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create">
    <div class="user-form">
    <section class="content">
      		<div class="row">
        	<!-- left column -->
        	<div class="col-md-6">
    		<div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= Html::encode($this->title) ?></h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
		    <?php $form = ActiveForm::begin(); ?>
		    <div class="box-body">


		    <?= $form->field($model, 'username')->textInput() ?>
		    <?= $form->field($model, 'password')->passwordInput() ?>
		    <?= $form->field($model, 'email')->input('email') ?>
		    <?= $form->field($model, 'role')->dropDownList(['1'=>'Admin','2'=>'Driver','3'=>'Customer']) ?> 
		    <?= $form->field($user_details, 'nama')->textInput() ?>
		    <?= $form->field($user_details, 'tanggal_lahir')->widget(
                            DatePicker::className(), [
                                // inline too, not bad
                                 'inline' => false,
                                 // modify template for custom rendering
                                //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                                'clientOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'value' => date('Y-m-d')
                                ]
                        ]); ?>
            <?= $form->field($user_details, 'jenis_kelamin')->dropDownList(['Perempuan' => 'Perempuan', 'Laki-Laki' => 'Laki-Laki'], ['prompt' => '- Pilih Jenis Kelamin -']); ?>
            <?= $form->field($user_details, 'no_hp')->textInput()->label('No Handphone'); ?>

 
 
		    </div>

		    <div class="box-footer">
		                <?= Html::submitButton('Add User', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
		    </div>

		    <?php ActiveForm::end(); ?>
		    </div>
		    </div>
		    </div>
		    </section>
    </div>

</div>
