<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Product;
use common\models\User;
use wbraganca\dynamicform\DynamicFormWidget;
use kartik\select2\Select2;
use dosamigos\datepicker\DatePicker;
use backend\assets\DatepickerAsset;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use yii\web\JsExpression;


/* @var $this yii\web\View */
/* @var $model app\models\Orders */
/* @var $form yii\widgets\ActiveForm */

$js='jQuery(".dynamicform_wrapper").on("afterInsert", function(e, item) {
        jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
            jQuery(this).html("Product: " + (index + 1))
        });
    });

    jQuery(".dynamicform_wrapper").on("afterDelete", function(e) {
        jQuery(".dynamicform_wrapper .panel-title-address").each(function(index) {
            jQuery(this).html("Product: " + (index + 1))
        });
    });
    ';

$this->registerJs($js);
?>

<div class="orders-form">
     <div class="row">
        <!-- left column -->
        <div class="col-md-12">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Orders</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <?php $form = ActiveForm::begin(['id' => 'orders-form']); ?>
            <div class="box-body">

            <?php DynamicFormWidget::begin([
                'widgetContainer' => 'dynamicform_wrapper', // required: only alphanumeric characters plus "_" [A-Za-z0-9_]
                'widgetBody' => '.container-items', // required: css class selector
                'widgetItem' => '.item', // required: css class
                'limit' => 5, // the maximum times, an element can be cloned (default 999)
                'min' => 0, // 0 or 1 (default 1)
                'insertButton' => '.add-item', // css class
                'deleteButton' => '.remove-item', // css class
                'model' => $modelDetails[0],
                'formId' => 'orders-form',
                'formFields' => [
                    'id_product',
                    'jumlah',
                ],
            ]); ?>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-envelope"></i> Product
                    <button type="button" class="pull-right add-item btn btn-success btn-xs"><i class="fa fa-plus"></i> Add Product</button>
                    <div class="clearfix"></div>
                </div>
                <div class="panel-body container-items"><!-- widgetContainer -->
                    <?php foreach ($modelDetails as $index => $modelDetails): ?>
                        <div class="item panel panel-default"><!-- widgetBody -->
                            <div class="panel-heading">
                                <span class="panel-title-address">Product: <?= ($index + 1) ?></span>
                                <button type="button" class="pull-right remove-item btn btn-danger btn-xs"><i class="fa fa-minus"></i></button>
                                <div class="clearfix"></div>
                            </div>
                            <div class="panel-body">
                                <?php
                                    // necessary for update action.
                                    if (!$modelDetails->isNewRecord) {
                                        echo Html::activeHiddenInput($modelDetails, "[{$index}]id");
                                    }
                                ?>
                                <?= $form->field($modelDetails, "[{$index}]id_product")->dropDownList(ArrayHelper::map(Product::find()->all(), 'id_product', 'nama'))->label('Product') ?>
                                <?= $form->field($modelDetails, "[{$index}]jumlah")->textInput(['maxlength' => true]) ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
            <?php DynamicFormWidget::end(); ?>


            <?= $form->field($model, 'tanggal')->widget(
                    DatePicker::className(), [
                        // inline too, not bad
                         'inline' => false,
                         // modify template for custom rendering
                        //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                        'clientOptions' => [
                            'autoclose' => true,
                            'format' => 'yyyy-mm-dd',
                            'value' => date('Y-m-d'),
                            'todayHighlight' => true, 
                        ]
                ]); ?>

             <?= $form->field($model, 'id_customer')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(User::find()->where('role = 3')->all(), 'id', 'username'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select Customer ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        ])->label('Customer'); ?>

            <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'lat')->textInput(['readOnly'=> true]) ?>

            <?= $form->field($model, 'lng')->textInput(['readOnly'=> true]) ?>
            <?php
                echo '<h3>Lokasi</h3>';
                echo "<div id='address'></div>";

                $model->isNewRecord ? $coord = new LatLng(['lat' => -7.24917, 'lng' => 112.75083]) : $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->lng]);

                //$coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]);
                $map = new Map([
                    'center' => $coord,
                    'zoom' => 14,
                    'width' => '100%',
                ]);
                $map->appendScript('
                    if(document.getElementById(\'orders-lat\').value != ""){
                         var myMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(document.getElementById(\'orders-lat\').value, document.getElementById(\'orders-lng\').value),
                            draggable: true
                        });
                    } else{
                        var myMarker = new google.maps.Marker({
                            position: new google.maps.LatLng(-7.24917, 112.75083),
                            draggable: true
                        });
                    }
                    
                    gmap0.setCenter(myMarker.position);
                    myMarker.setMap(gmap0);

                    google.maps.event.addListener(myMarker, \'dragend\', function(evt){
                        document.getElementById(\'orders-lat\').value = evt.latLng.lat();
                        document.getElementById(\'orders-lng\').value = evt.latLng.lng();
                        getReverseGeocodingData(evt.latLng.lat(),evt.latLng.lng());

                    });

                    function getReverseGeocodingData(lat, lng) {
                        var latlng = new google.maps.LatLng(lat, lng);
                        // This is making the Geocode request
                        var geocoder = new google.maps.Geocoder();
                        geocoder.geocode({ latLng: latlng }, function (results, status) {
                            if (status !== google.maps.GeocoderStatus.OK) {
                                alert(status);
                            }
                            // This is checking to see if the Geoeode Status is OK before proceeding
                            if (status == google.maps.GeocoderStatus.OK) {
                                console.log(results);
                                var address = (results[0].formatted_address);
                                document.getElementById(\'orders-alamat\').value = address;
                            }
                        });
                    }
                ');
                
                //$map->addOverlay($marker);
                 
                // Display the map -finally :)
                //ho "center : ".$map->getCenter();
                echo $map->display();
            ?>
            <br>

            <?= $form->field($model, 'status')->dropDownList(['Menunggu Pembayaran' => 'Menunggu Pembayaran', 'Verifikasi Pembayaran' => 'Verifikasi Pembayaran', 'Pembayaran Diterima' => 'Pembayaran Diterima', 'Pesanan Dikirim' => 'Pesanan Dikirim', 'Completed' => 'Completed', 'Dibatalkan' => 'Dibatalkan']);?>

            <div class="box-footer">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>


            <?php ActiveForm::end(); ?>
        </div>
    </div>
    </div>
    </div>
</div>
