<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;
use backend\models\OrderDetails;

/* @var $this yii\web\View */
/* @var $model app\models\Orders */

$this->title = $model->id_order;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-view">
    <div class="box">
        <div class="box-header">
        <?= Html::a('Update', ['update', 'id' => $model->id_order], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id_order], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        </div>
            <!-- /.box-header -->
        <div class="box-body no-padding">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'id_order',
                'tanggal',
                'total_berat',
                'total_harga',
                'id_customer',
                'alamat',
                'lat',
                'lng',
                // /'id_konfirmasi',
                'status',
            ],
        ]) ?>

        <br>
        <div class="form-group"> 
        <div style="padding:10px"><strong>Product :</strong></div>
        <?= GridView::widget([
                'dataProvider' => $model2,
                'summary'=>"", 
                'showFooter' => true,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    ['label'=>'Nama Product',
                    'value' => function($data){return $data->product->nama;}],
                    ['label' => 'Berat',
                    'value' => function($data){return $data->product->berat;}],
                    ['label' => 'Harga',
                    'value' => function($data){return $data->product->harga;}],
                    'jumlah',
                    ['label' => 'Total Berat',
                    'value' => function($data){return $data->product->berat * $data->jumlah;}],
                    ['attribute' => 'Total Harga',
                    'value' => function($data){return $data->product->harga * $data->jumlah;},
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
        </div>
    </div>

</div>
