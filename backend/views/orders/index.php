<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\OrdersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="orders-index">

    <div class="box">
            <div class="box-header">
               <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
                <!-- <p> -->
                    <?= Html::a('Create', ['create'], ['class' => 'btn btn-success']) ?>
                <!-- </p> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            <?= Datatables::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id_order',
                    'tanggal',
                    'total_berat',
                    'total_harga',
                    ['label'=>'Customer',
                        'value' => function($data){return $data->customer->username;}],
                    'alamat',
                    'lat',
                    'lng',
                    // 'id_konfirmasi',
                    'status',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
</div>
</div>
</div>
