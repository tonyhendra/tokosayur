<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Kategori */

$this->title = $model->nama_kategori;
$this->params['breadcrumbs'][] = ['label' => 'Kategoris', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-view">

     <section class="content">
      <div class="row">
        <div class="col-md-8">
         <div class="box">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id_kategori], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id_kategori], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id_kategori',
                        'nama_kategori',
                    ],
                ]) ?>
            </div>
        </div>
       </div>
     </div>
    </section>

</div>
