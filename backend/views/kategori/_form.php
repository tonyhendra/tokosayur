<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Kategori */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kategori-form">
	<section class="content">
      <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Kategori</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

		    <?php $form = ActiveForm::begin(); ?>
		    <div class="box-body">

		    <?= $form->field($model, 'nama_kategori')->textInput(['maxlength' => true]) ?>
		    </div>

		     <div class="box-footer">
		        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
		    </div>
		    <?php ActiveForm::end(); ?>
		    </div>
		    </div>
		    </div>
		    </section>

</div>
