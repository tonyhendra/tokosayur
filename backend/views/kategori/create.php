<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Kategori */

$this->title = 'Create Kategori';
$this->params['breadcrumbs'][] = ['label' => 'Kategoris', 'url' => ['index']];
?>
<div class="kategori-create">


    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
