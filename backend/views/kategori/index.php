<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;    

/* @var $this yii\web\View */
/* @var $searchModel app\models\KategoriSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Kategoris';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kategori-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="col-md-6">
     <div class="box">
            <div class="box-header">
               <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
                <!-- <p> -->
                    <?= Html::a('Add Kategori', ['create'], ['class' => 'btn btn-success']) ?>
                <!-- </p> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">

    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_kategori',
            'nama_kategori',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    </div>
</div>
