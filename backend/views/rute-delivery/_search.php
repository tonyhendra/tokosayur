<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RuteDeliverySearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-delivery-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id_rute') ?>

    <?= $form->field($model, 'id_kendaraan') ?>

    <?= $form->field($model, 'id_driver') ?>

    <?= $form->field($model, 'jumlah_kromosom') ?>

    <?= $form->field($model, 'jumlah_generasi') ?>

    <?php // echo $form->field($model, 'crossover_rate') ?>

    <?php // echo $form->field($model, 'mutation_rate') ?>

    <?php // echo $form->field($model, 'berat') ?>

    <?php // echo $form->field($model, 'rute') ?>

    <?php // echo $form->field($model, 'jarak') ?>

    <?php // echo $form->field($model, 'fitness_history') ?>

    <?php // echo $form->field($model, 'tanggal') ?>

    <?php // echo $form->field($model, 'status') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
