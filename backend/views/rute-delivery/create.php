<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RuteDelivery */

$this->title = 'Create Rute Delivery';
$this->params['breadcrumbs'][] = ['label' => 'Rute Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rute-delivery-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
