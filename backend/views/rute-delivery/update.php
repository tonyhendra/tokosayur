<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RuteDelivery */

$this->title = 'Update Rute Delivery: {nameAttribute}';
$this->params['breadcrumbs'][] = ['label' => 'Rute Deliveries', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_rute, 'url' => ['view', 'id' => $model->id_rute]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="rute-delivery-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
