<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RuteDelivery */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rute-delivery-form">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Rute</h3>
                </div>
                <!-- /.box-header -->
                <!-- form start -->
                <?php $form = ActiveForm::begin(); ?>

                <div class="box-body">

                    <?= $form->field($model, 'id_kendaraan')->textInput() ?>

                    <?= $form->field($model, 'id_driver')->textInput() ?>

                    <?= $form->field($model, 'jumlah_kromosom')->textInput() ?>

                    <?= $form->field($model, 'jumlah_generasi')->textInput() ?>

                    <?= $form->field($model, 'crossover_rate')->textInput() ?>

                    <?= $form->field($model, 'mutation_rate')->textInput() ?>

                    <?= $form->field($model, 'berat')->textInput() ?>

                    <?= $form->field($model, 'rute')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'total_jarak')->textInput() ?>

                    <?= $form->field($model, 'fitness_history')->textarea(['rows' => 6]) ?>

                    <?= $form->field($model, 'tanggal')->textInput() ?>

                    <?= $form->field($model, 'status')->textInput(['maxlength' => true]) ?>

                    <div class="form-group">
                        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</section>


</div>
