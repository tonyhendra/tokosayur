<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use backend\models\Kendaraan;
use common\models\User;

set_time_limit(1500);

/* @var $this yii\web\View */
/* @var $searchModel app\models\RuteDeliverySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */


$this->title = 'Rute Deliveries';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rute-delivery-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="box box-default">
        <div class="box-header with-border">
            <h3 class="box-title">Generate Rute Delivery</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-remove"></i></button>
            </div>
        </div>

        <!-- /.box-header -->
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                     <?php $form = ActiveForm::begin(['action' => 'index.php?r=rute-delivery/alga']); ?>
                    <div class="form-group">
                        <?= $form->field($model, 'id_kendaraan')->dropDownList(ArrayHelper::map(Kendaraan::find()->where('status = "Idle"')->all(), 'id_kendaraan', 'jenis_kendaraan'),[
                            'prompt'=>'- Pilih -',
                            ])->label('Kendaraan');?>
                        <?= $form->field($model, 'id_driver')->widget(Select2::classname(), [
                                'data' => ArrayHelper::map(User::find()->where('role = 2')->all(), 'id', 'username'),
                                'language' => 'en',
                                'options' => ['placeholder' => 'Select Driver ...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                ])->label('Driver'); ?>
                        <?= $form->field($model, 'jumlah_kromosom')->textInput()->label('Jumlah Kromosom yang Dibangkitkan') ?>
                        <?= $form->field($model, 'jumlah_generasi')->textInput() ?>
                        <?= $form->field($model, 'crossover_rate')->textInput(['value' => 0.75]) ?>
                        <?= $form->field($model, 'mutation_rate')->textInput(['value' => 0.25]) ?>

                
                    </div>
                </div>

            </div>
            <!-- /.col -->
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Submit</button>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <div class="box">

        <div class="box-header with-border">
            <h3 class="box-title">Rute</h3>
        </div>
        <div class="box-body">

            <?= DataTables::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'id_rute',
                    'id_kendaraan',
                    'id_driver',
                    //'jumlah_kromosom',
                    //'jumlah_generasi',
                    //'crossover_rate',
                    //'mutation_rate',
                    'berat',
                    'rute',
                    'total_jarak',
                    //'fitness_history:ntext',
                    'tanggal',
                    'status',

                    ['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
        </div>
    </div>
</div>
