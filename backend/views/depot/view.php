<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Depot */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Depots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depot-view">
  <section class="content">
        <div class="row">
            <div class="col-md-8">
                <div class="box">
                    <div class="box-header">
                        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body no-padding">


                        <?= DetailView::widget([
                            'model' => $model,
                            'attributes' => [
                                'id',
                                'alamat',
                                'lat',
                                'lng',
                            ],
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>
