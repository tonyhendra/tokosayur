<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;
use fedemotta\datatables\DataTables;


/* @var $this yii\web\View */
/* @var $searchModel app\models\DepotSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Depots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="depot-index">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Depot</h3>
                    </div>

                    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
                    <div style="padding: 10px">
                    <?= DataTables::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => 'yii\grid\SerialColumn'],

                            //'id',
                            'alamat',
                            'lat',
                            'lng',

                            ['class' => ActionColumn::className(),'template'=>'{view} {update}' ]
                        ],
                    ]); ?>
                </div>
                </div>
            </div>
        </div>
    </section>

</div>
