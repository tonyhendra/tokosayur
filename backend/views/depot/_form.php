<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Depot */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="depot-form">
	<section class="content">
	    <div class="row">
	        <!-- left column -->
	        <div class="col-md-6">
	          <!-- general form elements -->
	          	<div class="box box-primary">
	            	<div class="box-header with-border">
	              		<h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Depot</h3>
	            	</div>
	            	<!-- /.box-header -->
	            	<!-- form start -->
	            	<div class="box-body">
				    <?php $form = ActiveForm::begin(); ?>

				    <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

				    <?= $form->field($model, 'lat')->textInput(['maxlength' => true]) ?>

				    <?= $form->field($model, 'lng')->textInput(['maxlength' => true]) ?>

				    <div class="form-group">
				        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
				    </div>

				    <?php ActiveForm::end(); ?>
					</div>
				</div>
			</div>
		</div>
	</section>

</div>
