<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Depot */

$this->title = 'Update Depot';
$this->params['breadcrumbs'][] = ['label' => 'Depots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="depot-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
