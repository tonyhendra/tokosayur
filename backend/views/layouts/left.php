<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <!-- /.search form -->

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Menu', 'options' => ['class' => 'header']],
                    //['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii']],
                    //['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug']],
                    //['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                    /*[
                        'label' => 'Same tools',
                        'icon' => 'share',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Gii', 'icon' => 'file-code-o', 'url' => ['/gii'],],
                            ['label' => 'Debug', 'icon' => 'dashboard', 'url' => ['/debug'],],
                            [
                                'label' => 'Level One',
                                'icon' => 'circle-o',
                                'url' => '#',
                                'items' => [
                                    ['label' => 'Level Two', 'icon' => 'circle-o', 'url' => '#',],
                                    [
                                        'label' => 'Level Two',
                                        'icon' => 'circle-o',
                                        'url' => '#',
                                        'items' => [
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                            ['label' => 'Level Three', 'icon' => 'circle-o', 'url' => '#',],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],*/
                    ['label' => 'Product', 'icon' => 'bars', 'url' => '#',
                        'items' => [
                        ['label' => 'Manage Product', 'icon' => 'bars', 'url' => ['/product'],],
                        ['label' => 'Kategori', 'icon' => 'bars', 'url' => ['/kategori'],],
                        ],
                        ],
                    ['label' => 'Orders', 'icon' => 'bar-chart', 'url' => '#',
                        'items' => [
                            ['label' => 'Manage Orders', 'icon' => 'bar-chart', 'url' => ['/orders'],],
                            ['label' => 'Konfirmasi Pembayaran', 'icon' => 'bars', 'url' => ['/konfirmasi-pembayaran'],],
                        ],
                    ],                    
                    ['label' => 'Supply', 'icon' => 'bar-chart', 'url' => ['/supply']],
                    ['label' => 'User', 'icon' => 'users', 'url' => ['/user']],
                    ['label' => 'Petani', 'icon' => 'user', 'url' => ['/petani']],                    
                    //s['label' => 'Driver', 'icon' => 'user', 'url' => ['/country']],
                    ['label' => 'Rute', 'icon' => 'road', 'url' => '#',
                        'items' => [
                        ['label' => 'Rute Pickup', 'icon' => 'bars', 'url' => ['/rute-pickup'],],
                        ['label' => 'Rute Delivery', 'icon' => 'bars', 'url' => ['/rute-delivery'],],
                        ['label' => 'Kendaraan', 'icon' => 'bars', 'url' => ['/kendaraan'],],
                        ['label' => 'Depot', 'icon' => 'bars', 'url' => ['/depot'],],
                        
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
