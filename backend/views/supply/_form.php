<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use backend\models\Product;
use backend\models\Petani;
use dosamigos\datepicker\DatePicker;
use backend\assets\DatepickerAsset;
use kartik\select2\Select2;
/* @var $this yii\web\View */
/* @var $model app\models\Supply */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="supply-form">
    <section class="content">
        <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <!-- general form elements -->
              <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Supply</h3>
                </div>

                <?php $form = ActiveForm::begin(); ?>

                <div class="box-body">

                    <?= $form->field($model, 'id_product')->widget(Select2::classname(), [
                            'data' => ArrayHelper::map(Product::find()->all(), 'id_product', 'nama'),
                            'language' => 'en',
                            'options' => ['placeholder' => 'Select Product ...'],
                            'pluginOptions' => [
                                'allowClear' => true
                            ],
                            'pluginEvents' => [
                                'change' => "function() { console.log('data'); }",
                            ]
                        ])->label('Product'); ?>
                    <?= $form->field($model, 'id_petani')->widget(Select2::classname(), [
                        'data' => ArrayHelper::map(Petani::find()->all(), 'id_petani', 'nama'),
                        'language' => 'en',
                        'options' => ['placeholder' => 'Select Petani ...'],
                        'pluginOptions' => [
                            'allowClear' => true
                        ],
                        ])->label('Petani'); ?>
                    <!-- <div id="harga_jual">
                        <?= Html::textInput('harga_jual', 0); ?>
                    </div> -->
                    <?= $form->field($model, 'harga')->textInput() ?>
                    
                    <?= $form->field($model, 'jumlah')->textInput() ?>
                    <?= $form->field($model, 'tanggal')->widget(
                            DatePicker::className(), [
                                // inline too, not bad
                                 'inline' => false,
                                 // modify template for custom rendering
                                //'template' => '<div class="well well-sm" style="background-color: #fff; width:250px">{input}</div>',
                                'clientOptions' => [
                                    'autoclose' => true,
                                    'format' => 'yyyy-mm-dd',
                                    'value' => date('Y-m-d'),
                                    'todayHighlight' => true, 
                                ]
                        ]); ?>
                    <?= $form->field($model, 'status')->dropDownList(['Released' => 'Released', 'Canceled' => 'Canceled', 'Completed' => 'Completed']);?>
                </div>
                <div class="box-footer">
                    <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
    </section>
</div>

<!-- <?php
$this->registerJS('$("#supply-id_product").on("change", function (e) { 
                    var id = $("#supply-id_product").select2("data")[0].id_product;

                       // Now you can work with the selected value, i.e.:
                       $("#harga_jual").val(id);
                    });');
?> -->