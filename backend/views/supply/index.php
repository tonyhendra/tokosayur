<?php

use yii\helpers\Html;
use fedemotta\datatables\DataTables;

use backend\models\Product;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\SupplySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Supplies';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supply-index">
  <div class="box">
    <div class="box-header">
       <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
        <!-- <p> -->
        <?= Html::a('Create Supply', ['create'], ['class' => 'btn btn-success']) ?>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
    <?= DataTables::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'id_supply',
            ['label' => 'Product',
            'value' => function($data){return $data->product->nama;}],
            ['label'=>'Petani',
            'value' => function($data){return $data->petani->nama;}],
            'harga',
            ['label'=>'Berat/unit (gram)',
            'value' => function($data){return $data->berat;}],
            'jumlah',
             ['label'=>'Total Berat (Kg)',
            'value' => function($data){return $data->total_berat;}],
            [
                'attribute' => 'tanggal',
                'format' => ['date', 'php:d/m/Y']
            ],    
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
        'clientOptions' => [
                        "sScrollX" => true,
                         "responsive"=>true, 
                         
                    ],
    ]); ?>
</div>
</div>
</div>
