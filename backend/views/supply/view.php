<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Supply */
$this->title = $model->product->nama." - ".$model->petani->nama;
$this->params['breadcrumbs'][] = ['label' => 'Supplies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="supply-view">
<section class="content">
      <div class="row">
        <div class="col-md-8">
         <div class="box">

        <div class="box">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id_supply], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id_supply], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'id_supply',
                         ['label' => 'Product',
                            'value' => function($data){return $data->product->nama;}],                   
                         ['label' => 'Petani',
                            'value' => function($data){return $data->petani->nama;}],
                        'harga',
                        'berat',
                        'jumlah',
                        'total_berat',
                        'tanggal:date',
                        'status',
                    ],
                ]) ?>
            </div>
        </div>
     </div>
    </div>
    </div>
</section>

</div>
