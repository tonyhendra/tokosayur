<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Supply */

$this->title = 'Update Supply: ' . $model->product->nama." - ".$model->petani->nama;
$this->params['breadcrumbs'][] = ['label' => 'Supplies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->product->nama." - ".$model->petani->nama, 'url' => ['view', 'id' => $model->id_supply]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="supply-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
