<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Petani */

$this->title = $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Petanis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="petani-view">

    <div class="box">
            <div class="box-header">
                <?= Html::a('Update', ['update', 'id' => $model->id_petani], ['class' => 'btn btn-primary']) ?>
                <?= Html::a('Delete', ['delete', 'id' => $model->id_petani], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Are you sure you want to delete this item?',
                        'method' => 'post',
                    ],
                ]) ?>
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id_petani',
            'nama',
            'no_telp',
            'alamat',
            'lat',
            'lng',
            // ['label'=>'lat',
            //  'value' => function($data){return $data->pickupnode->lat;}],
            // ['label'=>'lng',
            //  'value' => function($data){return $data->pickupnode->lng;}],
             
        ],
    ]) ?>
    </div>

</div>
