<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Petani */

$this->title = 'Update Petani: ' . $model->nama;
$this->params['breadcrumbs'][] = ['label' => 'Petanis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->nama, 'url' => ['view', 'id' => $model->id_petani]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="petani-update">

    <?= $this->render('_form', [
        'model' => $model, 
        //'pickupnode' => $pickupnode
    ]) ?>

</div>
