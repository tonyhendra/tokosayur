<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\google\maps\LatLng;
use dosamigos\google\maps\services\DirectionsWayPoint;
use dosamigos\google\maps\services\TravelMode;
use dosamigos\google\maps\overlays\PolylineOptions;
use dosamigos\google\maps\services\DirectionsRenderer;
use dosamigos\google\maps\services\DirectionsService;
use dosamigos\google\maps\overlays\InfoWindow;
use dosamigos\google\maps\overlays\Marker;
use dosamigos\google\maps\Map;
use dosamigos\google\maps\services\DirectionsRequest;
use dosamigos\google\maps\overlays\Polygon;
use dosamigos\google\maps\layers\BicyclingLayer;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model app\models\Petani */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="petani-form">
    <div class="row">
        <!-- left column -->
        <div class="col-md-6">
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title"><?= $model->isNewRecord ? 'Add' : 'Update'?> Petani</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <?php $form = ActiveForm::begin(); ?>
            <div class="box-body">

                <?= $form->field($model, 'nama')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'no_telp')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'alamat')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'lat')->textInput(['readOnly'=> true]) ?>

                <?= $form->field($model, 'lng')->textInput(['readOnly'=> true]) ?>
                <?php
                    echo '<h3>Lokasi</h3>';
                    echo "<div id='address'></div>";

                    $model->isNewRecord ? $coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]) : $coord = new LatLng(['lat' => $model->lat, 'lng' => $model->lng]);

                    //$coord = new LatLng(['lat' => 39.720089311812094, 'lng' => 2.91165944519042]);
                    $map = new Map([
                        'center' => $coord,
                        'zoom' => 14,
                        'width' => '100%',
                    ]);
                    $map->appendScript('
                        if(document.getElementById(\'petani-lat\').value != ""){
                             var myMarker = new google.maps.Marker({
                                position: new google.maps.LatLng(document.getElementById(\'petani-lat\').value, document.getElementById(\'petani-lng\').value),
                                draggable: true
                            });
                        } else{
                            var myMarker = new google.maps.Marker({
                                position: new google.maps.LatLng(-7.24917, 112.75083),
                                draggable: true
                            });
                        }
                        
                        gmap0.setCenter(myMarker.position);
                        myMarker.setMap(gmap0);

                        google.maps.event.addListener(myMarker, \'dragend\', function(evt){
                            document.getElementById(\'petani-lat\').value = evt.latLng.lat();
                            document.getElementById(\'petani-lng\').value = evt.latLng.lng();
                            getReverseGeocodingData(evt.latLng.lat(),evt.latLng.lng());

                        });

                        function getReverseGeocodingData(lat, lng) {
                            var latlng = new google.maps.LatLng(lat, lng);
                            // This is making the Geocode request
                            var geocoder = new google.maps.Geocoder();
                            geocoder.geocode({ latLng: latlng }, function (results, status) {
                                if (status !== google.maps.GeocoderStatus.OK) {
                                    alert(status);
                                }
                                // This is checking to see if the Geoeode Status is OK before proceeding
                                if (status == google.maps.GeocoderStatus.OK) {
                                    console.log(results);
                                    var address = (results[0].formatted_address);
                                    document.getElementById(\'address\').innerHTML = address;
                                }
                            });
                        }
                    ');
                    
                    //$map->addOverlay($marker);
                     
                    // Display the map -finally :)
                    //ho "center : ".$map->getCenter();
                    echo $map->display();
                ?>
            </div>
            <div class="box-footer">
                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
            </div>
        </div>


        
    </div>
</div>
