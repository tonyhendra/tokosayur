<?php

use yii\helpers\Html;
use yii\grid\GridView;
use fedemotta\datatables\DataTables;

/* @var $this yii\web\View */
/* @var $searchModel app\models\PetaniSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Petanis';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="petani-index">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

      <div class="box">
            <div class="box-header">
               <!-- <h3 class="box-title">Data Table With Full Features</h3> -->
                <!-- <p> -->
                    <?= Html::a('Add Petani', ['create'], ['class' => 'btn btn-success']) ?>
                <!-- </p> -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <?= DataTables::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'nama',
                        'no_telp',
                        'alamat',
                        'lat',
                        'lng',
                        // ['label'=>'lat',
                        //  'value' => function($data){return $data->pickupnode->lat;}],
                        // ['label'=>'lng',
                        //  'value' => function($data){return $data->pickupnode->lng;}],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                    'clientOptions' => [
                        "sScrollX" => true,
                         "responsive"=>true, 
                         
                    ],
                ]); ?>
            </div>
    </div>
</div>
