<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Petani */

$this->title = 'Create Petani';
$this->params['breadcrumbs'][] = ['label' => 'Petanis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="petani-create">

    <!-- <h1><?= Html::encode($this->title) ?></h1> -->

    <?= $this->render('_form', [
        'model' => $model, 
        //'pickupnode' => $pickupnode
    ]) ?>


</div>

