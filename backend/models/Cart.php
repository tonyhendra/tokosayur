<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property int $id_cart
 * @property string $username
 * @property int $id_product
 * @property int $jumlah
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'id_product', 'jumlah'], 'required'],
            [['id_product', 'jumlah'], 'integer'],
            [['username'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_cart' => 'Id Cart',
            'username' => 'Username',
            'id_product' => 'Id Product',
            'jumlah' => 'Jumlah',
        ];
    }
}
