<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Supply;

/**
 * SupplySearch represents the model behind the search form about `\app\models\Supply`.
 */
class SupplySearch extends Supply
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_supply', 'id_product', 'id_petani', 'harga', 'jumlah'], 'integer'],
            [['berat', 'total_berat'], 'double'],
            [['tanggal', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Supply::find();

        // add conditions that should always apply here
        $query->joinWith(['petani']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_supply' => $this->id_supply,
            'id_product' => $this->id_product,
            'id_petani' => $this->id_petani,
            'harga' => $this->harga,
            'berat' => $this->berat,
            'jumlah' => $this->jumlah,
            'total_berat' => $this->total_berat,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
