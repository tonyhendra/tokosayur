<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\KonfirmasiPembayaran;

/**
 * KonfirmasiPembayaranSearch represents the model behind the search form of `backend\models\KonfirmasiPembayaran`.
 */
class KonfirmasiPembayaranSearch extends KonfirmasiPembayaran
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_order'], 'integer'],
            [['nominal_transfer'], 'number'],
            [['nama', 'no_rek', 'bank', 'tujuan_transfer', 'tanggal', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KonfirmasiPembayaran::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_order' => $this->id_order,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'no_rek', $this->no_rek])
            ->andFilterWhere(['like', 'bank', $this->bank])
            ->andFilterWhere(['like', 'nominal_transfer', $this->nominal_transfer])
            ->andFilterWhere(['like', 'tujuan_transfer', $this->tujuan_transfer])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
