<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Kendaraan;

/**
 * KendaraanSearch represents the model behind the search form about `app\models\Kendaraan`.
 */
class KendaraanSearch extends Kendaraan
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kendaraan', 'id_driver'], 'integer'],
            [['jenis_kendaraan', 'nomor_kendaraan'], 'safe'],
            [['kapasitas'], 'number'],
            [['status'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Kendaraan::find();

        // add conditions that should always apply here
        $query->joinWith(['driver']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_kendaraan' => $this->id_kendaraan,
            'jenis_kendaraan' => $this->jenis_kendaraan,
            'kapasitas' => $this->kapasitas,
            'id_driver' => $this->id_driver,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'jenis_kendaraan', $this->jenis_kendaraan])
            ->andFilterWhere(['like', 'nomor_kendaraan', $this->nomor_kendaraan])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
