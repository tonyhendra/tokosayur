<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RutePickup;

/**
 * RutePickupSearch represents the model behind the search form about `app\models\RutePickup`.
 */
class RutePickupSearch extends RutePickup
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rute', 'id_kendaraan', 'id_driver', 'jumlah_kromosom', 'jumlah_generasi'], 'integer'],
            [['berat', 'total_jarak', 'crossover_rate','mutation_rate'], 'number'],
            [['rute', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RutePickup::find();

        $query->joinWith(['kendaraan', 'driver']);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rute' => $this->id_rute,
            'id_kendaraan' => $this->id_kendaraan,
            'id_driver' => $this->id_driver,
            'jumlah_kromosom' => $this->jumlah_kromosom,
            'jumlah_generasi' => $this->jumlah_generasi,
            'crossover_rate' => $this->crossover_rate,
            'mutation_rate' => $this->mutation_rate,
            'berat' => $this->berat,
            'total_jarak' => $this->total_jarak,
        ]);

        $query->andFilterWhere(['like', 'rute', $this->rute])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
