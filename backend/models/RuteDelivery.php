<?php

namespace backend\models;

use Yii;
use \common\models\User;
/**
 * This is the model class for table "rute_delivery".
 *
 * @property int $id_rute
 * @property int $id_kendaraan
 * @property int $id_driver
 * @property int $jumlah_kromosom
 * @property int $jumlah_generasi
 * @property double $crossover_rate
 * @property double $mutation_rate
 * @property double $berat
 * @property string $rute
 * @property double $jarak
 * @property string $fitness_history
 * @property string $tanggal
 * @property string $status
 */
class RuteDelivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rute_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_kendaraan', 'id_driver', 'jumlah_kromosom', 'jumlah_generasi', 'crossover_rate', 'mutation_rate', 'berat', 'rute', 'total_jarak', 'fitness_history', 'tanggal', 'status'], 'required'],
            [['id_kendaraan', 'id_driver', 'jumlah_kromosom', 'jumlah_generasi'], 'integer'],
            [['crossover_rate', 'mutation_rate', 'berat', 'total_jarak'], 'number'],
            [['fitness_history'], 'string'],
            [['tanggal'], 'safe'],
            [['rute'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_rute' => 'Id Rute',
            'id_kendaraan' => 'Id Kendaraan',
            'id_driver' => 'Id Driver',
            'jumlah_kromosom' => 'Jumlah Kromosom',
            'jumlah_generasi' => 'Jumlah Generasi',
            'crossover_rate' => 'Crossover Rate',
            'mutation_rate' => 'Mutation Rate',
            'berat' => 'Berat',
            'rute' => 'Rute',
            'total_jarak' => 'Total Jarak',
            'fitness_history' => 'Fitness History',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
        ];
    }

    public function getKendaraan()
    {
        return $this->hasOne(Kendaraan::className(), ['id_kendaraan' => 'id_kendaraan']);
    }

    public function getDriver()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'id_driver']);
    }

    public function randKromosom($data, $jumlah_kromosom, $kendaraan)
    {
        $rand_kromosom = [];
        for ($i=0; $i <$jumlah_kromosom; $i++) { 
            # code...

            $random_key = array_rand($data,count($data));
            $rand_temp = [];

            //$total_berat = 0;
            for($j=0; $j<count($data); $j++){  
            //    $berat = $this->getBerat($data[$random_key[$j]]);
            //    $total_berat = $total_berat+$berat;
            //    if($total_berat<=$this->getKapasitas($kendaraan)){
                    array_push($rand_temp, $data[$random_key[$j]]);    
            //    }
                    
            }
            
            $count = count($rand_kromosom);
            if($count > 0){
                for($k=0; $k<$count;$k++){
                    //echo "j = ".$j."<br>";
                    while($rand_temp==$rand_kromosom[$k]){
                        shuffle($rand_temp);
                        //echo "sama </br>";
                        $k=0;
                    }
                }
                array_push($rand_kromosom, $rand_temp);
            }else{
                array_push($rand_kromosom, $rand_temp);
            }
            shuffle($data);

        }
        return $rand_kromosom;

    }

    public function fitness($rand_kromosom){
        
        $lat_depot = $this->getLatDepot();
        $lng_depot = $this->getLngDepot();
        $jarak = [];
        for($a=0; $a<count($rand_kromosom);$a++){
            $total;
            for($b=0; $b<count($rand_kromosom[$a]);$b++){
                //Jika b=0, jika b merupakan titik awal maka akan dihitung jarak antara depot dengan titik awal
                if($b==0){
                    $total=$this->distance($lat_depot,$lng_depot,$this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]), "K");
                }
                //jika b merupaka titik terakhir maka akan dihitung jarak antara titik akhir dengan depot
                elseif ($b==count($rand_kromosom[$a])-1) {
                    # code...
                    $total=$total+$this->distance($this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]),$lat_depot,$lng_depot, "K");
                }
                //menghitung jarak antara titik satu dengan titik selanjutnya
                else{
                    $total=$total+$this->distance($this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]),$this->getLat($rand_kromosom[$a][$b+1]),$this->getLng($rand_kromosom[$a][$b+1]), "K");
                }
            }
            array_push($jarak, $total);
        }
        
        $fitness_array = [];
        for ($i=0; $i < count($jarak); $i++) { 
            # code...
            $fitness_kromosom = 1/$jarak[$i];
            array_push($fitness_array, $fitness_kromosom);
        }
        return $fitness_array;

    }


    public function probFitness($fitness_array){       
        $total_fitness = array_sum($fitness_array);
        $prob_fitness_array = [];
        for ($i=0; $i < count($fitness_array); $i++) { 
            $prob_fitness = $fitness_array[$i]/$total_fitness;
            array_push($prob_fitness_array, $prob_fitness);
        }
        return $prob_fitness_array;
    }

    //Untuk proses seleksi menggunaka roulete wheel, untuk itu harus dicari dahulu nilai kumulatif probabilitasnya
    public function probkumFitness($prob_fitness_array){
        $probkum_fitness_array = [];
        $probkum_fitness = 0.0;
        for ($i=0; $i < count($prob_fitness_array); $i++) { 
            $probkum_fitness = $probkum_fitness+$prob_fitness_array[$i];
            array_push($probkum_fitness_array, $probkum_fitness);
        }
        return $probkum_fitness_array;
    }

    public function seleksiRws($probkum_fitness_array, $rand_kromosom){
        $new_pop = [];
        for($x=0;$x<count($probkum_fitness_array);$x++){
            $rand_rws = (float)rand()/(float)getrandmax();
            for($i=0;$i<count($probkum_fitness_array);$i++){
                if($i==0){      
                    if($rand_rws<$probkum_fitness_array[$i]){
                        $new_pop[$x] = $rand_kromosom[$i];
                    }
                    
                }else{
                    if($rand_rws>$probkum_fitness_array[$i-1] && $rand_rws<$probkum_fitness_array[$i]){
                        $new_pop[$x] = $rand_kromosom[$i];
                    }
                }
            }
        }
        return $new_pop;
    }
    public function indukCrossOver($new_pop, $crossover_rate){
        $induk_crossover = [];
        for($i=0; $i<count($new_pop); $i++){
            $rand_crossover = (float)rand()/(float)getrandmax();
            if($rand_crossover < $crossover_rate){
                array_push($induk_crossover, $new_pop[$i]);
            }
        }
        return $induk_crossover;
    }

    public function crossover($parent1, $parent2){
        $startPos = rand(0, count($parent1)/2-1);
        $endPos = rand(count($parent1)/2+1, count($parent1));
        
        $offspring = [];
        for($i = 0; $i < count($parent1); $i++){
            if($startPos < $endPos && $i > $startPos && $i < $endPos){
                $offspring = $offspring+array_fill($i, 1, $parent1[$i]);
            }else if($startPos > $endPos){
                if(!($i < $startPos && $i > $endPos)){
                    $offspring = $offspring+array_fill($i, 1, $parent1[$i]);
                }
            }
        }

        for($i = 0; $i < count($parent2); $i++){
            if(!in_array($parent2[$i], $offspring)){
                for($j = 0; $j < count($parent2); $j++){
                    if(empty($offspring[$j])){
                        $offspring = $offspring+array_fill($j, 1, $parent2[$i]);
                        break;
                    }
                }
            }
        }
        ksort($offspring);
        return $offspring;
    }

    public function mutationExchange($new_pop, $mutation_rate){
        $pop_mutation = $new_pop;
        for($i=0; $i<count($pop_mutation); $i++){
            $rand_mutation= (float)rand()/(float)getrandmax();
            if($rand_mutation < $mutation_rate){
                $pos1 = rand(0, count($pop_mutation[$i])-1);
                $pos2 = rand(0, count($pop_mutation[$i])-1);
                $temp = $pop_mutation[$i][$pos1];
                $pop_mutation[$i][$pos1] = $pop_mutation[$i][$pos2];
                $pop_mutation[$i][$pos2] = $temp;
            }
        }
        return $pop_mutation;
    }
    public function seleksiElitism($new_pop, $offspring, $pop_mutation){
        $elitism = [];
        for($i=0; $i<count($new_pop);$i++){
            array_push($elitism, $new_pop[$i]);
        }
        for($i=0; $i<count($offspring);$i++){
            array_push($elitism, $offspring[$i]);
        }
        
        for($i=0; $i<count($pop_mutation);$i++){
            array_push($elitism, $pop_mutation[$i]);
        }        $elitism_fitness = $this->fitness($elitism);
        arsort($elitism_fitness);
        $elitism_new_pop = [];
        foreach($elitism_fitness as $x => $x_value) {
            if(count($elitism_new_pop) < count($new_pop)){
                array_push($elitism_new_pop, $elitism[$x]);
            }
        }
        return $elitism_new_pop;
    }

    public function alga($rand_kromosom, $crossover_rate, $mutation_rate){
        $fitness_array = $this->fitness($rand_kromosom);
        $prob_fitness_array = $this->probFitness($fitness_array);
        $probkum_fitness_array = $this->probkumFitness($prob_fitness_array);
        $new_pop = $this->seleksiRws($probkum_fitness_array, $rand_kromosom);
        $induk_crossover = $this->indukCrossOver($new_pop, $crossover_rate);
        $offspring = [];
        if(count($induk_crossover)>1){
            for($i=0; $i < count($induk_crossover); $i++){
                if($i < count($induk_crossover)-1){
                    array_push($offspring,$this->crossover($induk_crossover[$i], $induk_crossover[$i+1]));
                }else{
                    array_push($offspring, $this->crossover($induk_crossover[$i], $induk_crossover[0]));
                }
            }
        }
        $pop_mutation = $this->mutationExchange($new_pop, $mutation_rate);
        $elitism_new_pop = $this->seleksiElitism($rand_kromosom, $offspring, $pop_mutation);
        return $elitism_new_pop;
        
    }

    public function cariJarak($rand_kromosom){
    $jarak = [];
    $lat_depot = $this->getLatDepot();
    $lng_depot = $this->getLngDepot();
    
    for($a=0; $a<count($rand_kromosom);$a++){
        $total;
        for($b=0; $b<count($rand_kromosom[$a]);$b++){
            if($b==0){
                $total=$this->distance($lat_depot,$lng_depot,$this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]), "K");
            }elseif ($b==count($rand_kromosom[$a])-1) {
                # code...
                $total=$total+$this->distance($this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]),$lat_depot,$lng_depot, "K");
            }else{
                $total=$total+$this->distance($this->getLat($rand_kromosom[$a][$b]),$this->getLng($rand_kromosom[$a][$b]),$this->getLat($rand_kromosom[$a][$b+1]),$this->getLng($rand_kromosom[$a][$b+1]), "K");
            }
        }
        array_push($jarak, $total);
    }
    return $jarak;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function getLat($id){
        $result = Orders::find()->select(['lat',])->where(['id_order'=>$id,])->asarray()->one();
        return $result['lat'];
    }

    public function getLng($id){
        $result = Orders::find()->select(['lng',])->where(['id_order'=>$id,])->asarray()->one();
        return $result['lng'];
    }

    public function getBerat($id_order){
        $result = Orders::find()->select(['total_berat',])->where(['id_order'=>$id_order])->asarray()->one();
        return $result['total_berat'];
    }

    public function getKapasitas($id_kendaraan){
        $result = Kendaraan::find()->select(['kapasitas',])->where(['id_kendaraan'=>$id_kendaraan, ])->asarray()->one();
        return $result['kapasitas'];
    }

    public function getAlamat($id){
        $result = Orders::find()->select(['alamat',])->where(['id_order'=>$id,])->asarray()->one();
        return $result['alamat'];
    }
    public function getNama($id){
        $order = Orders::find()->select(['id_customer',])->where(['id_order'=>$id,])->asarray()->one();
        $user = User::find()->select(['username',])->where(['id' => $order['id_customer'],])->asarray()->one();
        $user_detail = UserDetails::find()->select(['nama',])->where(["username" => $user["username"],])->asarray()->one();
        return $user_detail['nama'];
    }

    public function getProduct($id){
        $order = OrderDetails::find()->select(['id_product',])->where(['id_order'=>$id,])->asarray()->one();
        return OrderDetails::getProduct($order['id_product']);
    }

    public function getJumlah($id){
        $order = OrderDetails::find()->select(['jumlah',])->where(['id_order'=>$id,])->asarray()->one();
        return $order['jumlah'];
    }


    public function getLatDepot(){
        $result = Depot::find()->asarray()->one();
        return $result['lat'];
    }
    public function getLngDepot(){
        $result = Depot::find()->asarray()->one();
        return $result['lng'];
    }
    public function getAlamatDepot(){
        $result = Depot::find()->asarray()->one();
        return $result['alamat'];
    }
}
