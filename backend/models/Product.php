<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property integer $id_product
 * @property string $nama
 * @property string $deskripsi
 * @property integer $harga
 * @property integer $stock
 * @property integer $id_kategori
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'deskripsi', 'harga', 'stock', 'id_kategori', 'berat', 'terjual'], 'required'],
            [['deskripsi'], 'string'],
            [['harga', 'stock', 'id_kategori', 'terjual'], 'integer'],
            [['berat'], 'double'],
            [['nama'], 'string', 'max' => 50],
            [['id_kategori'], 'default', 'value' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_product' => 'Id Product',
            'nama' => 'Nama',
            'deskripsi' => 'Deskripsi',
            'berat' => 'Berat',
            'harga' => 'Harga',
            'stock' => 'Stock',
            'terjual' => 'Terjual',
            'id_kategori' => 'Id Kategori',
        ];
    }
    public function getKategori()
    {
        return $this->hasOne(Kategori::className(), ['id_kategori' => 'id_kategori']);
    }
}
