<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "kendaraan".
 *
 * @property integer $id_kendaraan
 * @property string $jenis kendaraan
 * @property double $kapasitas
 * @property string $nomor_kendaraan
 * @property integer $id_driver
 */
class Kendaraan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kendaraan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['jenis_kendaraan', 'kapasitas', 'nomor_kendaraan', 'id_driver', 'status'], 'required'],
            [['kapasitas'], 'number'],
            [['id_driver'], 'integer'],
            [['jenis_kendaraan'], 'string', 'max' => 50],
            [['nomor_kendaraan'], 'string', 'max' => 15],
            [['status'], 'string', 'max' => 20],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_kendaraan' => 'Id Kendaraan',
            'jenis_kendaraan' => 'Jenis Kendaraan',
            'kapasitas' => 'Kapasitas',
            'nomor_kendaraan' => 'Nomor Kendaraan',
            'id_driver' => 'Id Driver',
            'status' => 'Status',
        ];
    }

     public function getDriver()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'id_driver']);
    }
}
