<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "konfirmasi_pembayaran".
 *
 * @property int $id
 * @property int $id_order
 * @property string $nama
 * @property string $no_rek
 * @property string $bank
 * @property string $tujuan_transfer
 * @property string $tanggal
 * @property string $status
 *
 * @property Orders $order
 */
class KonfirmasiPembayaran extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'konfirmasi_pembayaran';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order', 'nama', 'no_rek', 'bank', 'tujuan_transfer', 'tanggal', 'status'], 'required'],
            [['id_order'], 'integer'],
            [['tanggal'], 'safe'],
            [['nama'], 'string', 'max' => 250],
            [['no_rek', 'bank', 'tujuan_transfer', 'status'], 'string', 'max' => 50],
            [['nominal_transfer'], 'number'],
            [['id_order'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['id_order' => 'id_order']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_order' => 'Id Order',
            'nama' => 'Nama',
            'no_rek' => 'No Rekening',
            'bank' => 'Bank',
            'nominal_transfer' => 'Nominal Transfer',
            'tujuan_transfer' => 'Tujuan Transfer',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id_order' => 'id_order']);
    }
}
