<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "user_details".
 *
 * @property string $username
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $no_hp
 */
class UserDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username'], 'required'],
            [['tanggal_lahir'], 'safe'],
            [['username'], 'string', 'max' => 100],
            [['nama'], 'string', 'max' => 200],
            [['jenis_kelamin'], 'string', 'max' => 20],
            [['no_hp'], 'string', 'max' => 15],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => 'Username',
            'nama' => 'Nama',
            'tanggal_lahir' => 'Tanggal Lahir',
            'jenis_kelamin' => 'Jenis Kelamin',
            'no_hp' => 'No Hp',
        ];
    }
}
