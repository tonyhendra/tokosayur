<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "orders".
 *
 * @property integer $id_order
 * @property string $tanggal
 * @property double $harga_total
 * @property integer $id_customer
 * @property string $alamat
 * @property double $lat
 * @property double $lng
 * @property integer $id_konfirmasi
 * @property string $status
 */
class Orders extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tanggal', 'id_customer', 'alamat', 'lat', 'lng', 'status'], 'required'],
            [['tanggal'], 'safe'],
            [['total_harga', 'total_berat', 'lat', 'lng'], 'number'],
            [['id_customer', ], 'integer'],
            [['alamat'], 'string', 'max' => 250],
            [['status'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_order' => 'Id Order',
            'tanggal' => 'Tanggal',
            'total_berat' => 'Total Berat',
            'total_harga' => 'Total Harga',
            'id_customer' => 'Id Customer',
            'alamat' => 'Alamat',
            'lat' => 'Lat',
            'lng' => 'Lng',
            //'id_konfirmasi' => 'Id Konfirmasi',
            'status' => 'Status',
        ];
    }

    public function getOrderDetails()
    {
        return $this->hasMany(OrderDetails::className(), ['id_order' => 'id_order']);
    }
 
    public function setOrderDetails($value)
    {
        $this->loadRelated('orderDetails', $value);
    }

    public function getCustomer()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'id_customer']);
    }
}
