<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "order_details".
 *
 * @property integer $id
 * @property integer $id_order
 * @property integer $id_product
 * @property integer $jumlah
 * @property string $harga
 */
class OrderDetails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'jumlah'], 'required'],
            [['id_product', 'jumlah'], 'integer'],
            [['id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'id_order' => 'Id Order',
            'id_product' => 'Id Product',
            'jumlah' => 'Jumlah',

        ];
    }

    public function getHarga($id){
        $result = Product::find()->select(['harga',])->where(['id_product'=>$id,])->asarray()->one();
        return $result['harga'];
    }

    public function getBerat($id){
        $result = Product::find()->select(['berat',])->where(['id_product'=>$id,])->asarray()->one();
        return $result['berat'];
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    public function getNamaProduct($id)
    {
        //return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
        $result = Product::find()->select(['nama',])->where(['id_product'=>$id,])->asarray()->one();
        return $result['nama'];
    }

    public static function getTotal($provider, $columnName)
    {
        $total = 0;
        foreach ($provider as $item) {
          $total += $item[$columnName];
      }
      return $total;  
    }
}
