<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "supply".
 *
 * @property integer $id
 * @property integer $id_product
 * @property integer $id_petani
 * @property integer $harga
 * @property integer $jumlah
 * @property string $tanggal
 * @property string $status
 */
class Supply extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'supply';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'id_petani', 'harga', 'jumlah', 'tanggal', 'status'], 'required'],
            [['id_product', 'id_petani', 'harga', 'jumlah'], 'integer'],
            [['berat', 'total_berat'], 'double'],
            [['tanggal'], 'safe'],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_supply' => 'Id Supply',
            'id_product' => 'Id Product',
            'id_petani' => 'Id Petani',
            'harga' => 'Harga',
            'berat' => 'Berat',
            'jumlah' => 'Jumlah',
            'total_berat' => 'Total Berat',
            'tanggal' => 'Tanggal',
            'status' => 'Status',
        ];
    }

    public function getPetani()
    {
        return $this->hasOne(Petani::className(), ['id_petani' => 'id_petani']);
    }

    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id_product' => 'id_product']);
    }

    public function getNamaProduct($id)
    {
        $result = Product::find()->select(['nama',])->where(['id_product'=>$id,])->asarray()->one();
        return $result['nama'];
    }


    public function getProductBerat($id){
        $result = Product::find()->select(['berat',])->where(['id_product'=>$id,])->asarray()->one();
        return $result['berat'];
    }

    public function getHarga($id_product){
        $result = Product::find()->select(['harga',])->where(['id_product'=>$id_product,])->asarray()->one();
        return $result['harga'];
    }
}
