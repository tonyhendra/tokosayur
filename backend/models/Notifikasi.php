<?php

namespace backend\models;

use Yii;
use backend\models\Orders;
use common\models\User;

/**
 * This is the model class for table "notifikasi".
 *
 * @property int $id_notifikasi
 * @property int $id_order
 * @property int $id_customer
 * @property string $pesan
 * @property string $tanggal
 *
 * @property Orders $order
 * @property User $user
 */
class Notifikasi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notifikasi';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_order', 'id_customer', 'pesan', 'tanggal'], 'required'],
            [['id_order', 'id_customer'], 'integer'],
            [['tanggal'], 'safe'],
            [['pesan'], 'string', 'max' => 250],
            [['id_order'], 'exist', 'skipOnError' => true, 'targetClass' => Orders::className(), 'targetAttribute' => ['id_order' => 'id_order']],
            [['id_customer'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['id_customer' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_notifikasi' => 'Id Notifikasi',
            'id_order' => 'Id Order',
            'id_customer' => 'Id User',
            'pesan' => 'Pesan',
            'tanggal' => 'Tanggal',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Orders::className(), ['id_order' => 'id_order']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'id_customer']);
    }
}
