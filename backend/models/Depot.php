<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "depot".
 *
 * @property int $id
 * @property string $alamat
 * @property string $lat
 * @property string $lng
 */
class Depot extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'depot';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['alamat', 'lat', 'lng'], 'required'],
            [['lat', 'lng'], 'number'],
            [['alamat'], 'string', 'max' => 250],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alamat' => 'Alamat',
            'lat' => 'Lat',
            'lng' => 'Lng',
        ];
    }
}
