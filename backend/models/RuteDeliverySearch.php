<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\RuteDelivery;

/**
 * RuteDeliverySearch represents the model behind the search form of `app\models\RuteDelivery`.
 */
class RuteDeliverySearch extends RuteDelivery
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_rute', 'id_kendaraan', 'id_driver', 'jumlah_kromosom', 'jumlah_generasi'], 'integer'],
            [['crossover_rate', 'mutation_rate', 'berat', 'total_jarak'], 'number'],
            [['rute', 'fitness_history', 'tanggal', 'status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = RuteDelivery::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_rute' => $this->id_rute,
            'id_kendaraan' => $this->id_kendaraan,
            'id_driver' => $this->id_driver,
            'jumlah_kromosom' => $this->jumlah_kromosom,
            'jumlah_generasi' => $this->jumlah_generasi,
            'crossover_rate' => $this->crossover_rate,
            'mutation_rate' => $this->mutation_rate,
            'berat' => $this->berat,
            'jtotal_arak' => $this->total_jarak,
            'tanggal' => $this->tanggal,
        ]);

        $query->andFilterWhere(['like', 'rute', $this->rute])
            ->andFilterWhere(['like', 'fitness_history', $this->fitness_history])
            ->andFilterWhere(['like', 'status', $this->status]);

        return $dataProvider;
    }
}
