<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Product;

/**
 * ProductSeacrh represents the model behind the search form about `app\models\Product`.
 */
class ProductSearch extends Product
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_product', 'harga', 'stock', 'id_kategori', 'terjual'], 'integer'],
            [['berat'], 'double'],
            [['nama', 'deskripsi'], 'safe'],
            [['id_kategori'], 'default', 'value' => ''],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Product::find();

        // add conditions that should always apply here
        $query->joinWith(['kategori']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_product' => $this->id_product,
            'berat' => $this->berat,
            'harga' => $this->harga,
            'terjual' => $this->terjual,
            'product.id_kategori' => $this->id_kategori,
        ]);

        if(isset($_GET['limit'])){
            $query->limit($_GET['limit']);    
        }
        


        $query->andFilterWhere(['like', 'nama', $this->nama])
            ->andFilterWhere(['like', 'deskripsi', $this->deskripsi])->andFilterWhere(['>', 'stock', $this->stock]);

        return $dataProvider;
    }
}
