<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "petani".
 *
 * @property integer $id
 * @property string $nama
 * @property string $no_telp
 * @property string $alamat
 * @property double $lat
 * @property double $lng
 */
class Petani extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'petani';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['nama', 'no_telp', 'alamat', 'lat', 'lng'], 'required'],
            [['nama'], 'string', 'max' => 50],
            [['no_telp'], 'string', 'max' => 13],
            [['alamat'], 'string', 'max' => 200],
            [['lat', 'lng'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_petani' => 'Id Petani',
            'nama' => 'Nama',
            'no_telp' => 'No Telp',
            'alamat' => 'Alamat',
            'lat' => 'Lat',
            'lng' => 'Lng',

        ];
    }
}
